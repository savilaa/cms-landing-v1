<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Becas extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('becas_model');
    } 
    
	//public function index()
//	{
//		echo"dsdsdsds";
//        
//        $data->content = $this->load->view('index.php',null,TRUE);
//		$this->load->view('html.tpl.php',$data);
//
//	}
    
    
    public function view_becas(){

        $aux_data['title']="Programa";
        $aux_data['texto']="¿Qué quieres estudiar?";
        $aux_data['texto2']="Maestrías";
        $aux_data['texto_select']="Selecciona tu Programa";
        $aux_data['texto_nivel']="Selecciona tu nivel";

        $campus=$this->becas_model->get_campus();
        $aux_data['campus']=$campus->result_array();
        $aux_data['gracias']=base_url()."becas-apoyos-financieros/gracias";

        $aux_form['content']=$this->load->view('view_form.php',$aux_data,TRUE);
        $aux_form['header']=$this->load->view('tpl.header2.php',null,TRUE);
        $aux_form['footer']=$this->load->view('tpl.footer2.php',null,TRUE);
        $data['content'] = $this->load->view('view_becas.php',$aux_form,TRUE);
        $this->load->view('html.tpl.php',$data);
    }
    
    public function becas_gracias(){
        $this->load->view('becas_gracias.php');
    }

    public function get_campus_BY_programa_and_nivel(){
        if(isset($_POST['id_nivel_interes']) AND isset($_POST['id_programa_interes'])){
            $id_nivel_interes=$_POST['id_nivel_interes'];
            $id_programa_interes=$_POST['id_programa_interes'];
            $list_campus=$this->masthead_model->get_campus_BY_programa_and_nivel($id_nivel_interes,$id_programa_interes);
            print_r(json_encode($list_campus));
        }
    }
    
    
    
    
    
  public function get_progrma_BY_campus_and_nivel(){
    
        if(isset($_POST['id_nivel_interes']) AND isset($_POST['id_campus'])){
            $id_nivel_interes=$_POST['id_nivel_interes'];
            $id_campus=$_POST['id_campus'];
            $aux_list_campus=$this->masthead_model->get_progrma_BY_campus_and_nivel($id_nivel_interes,$id_campus);
            $list_campus=$aux_list_campus->result_array();
            print_r(json_encode($list_campus));
        }
    
    
  }

    public function get_progrma_by_nivel(){
    
        if(isset($_POST['id_nivel_interes']) AND isset($_POST['nivel_interes'])){
            $id_nivel_interes=$_POST['id_nivel_interes'];
            $nivel_interes=$_POST['nivel_interes'];
            $programa_interes=$this->masthead_model->get_programa_interes_by_nivel_interes($id_nivel_interes,$nivel_interes);
            $list_campus=$programa_interes->result_array();
            print_r(json_encode($list_campus));
        }
    
    
  }
  
  
  
  public function get_mapa(){
    if(isset($_POST['id_campus'])){
        $id_campus=$_POST['id_campus'];
        $aux_list_campus=$this->masthead_model->get_mapa($id_campus);
        $list_campus=$aux_list_campus->result_array();
        print_r(json_encode($list_campus));
    }
    
  }
  
  
  
  public function get_campus(){
    if(isset($_GET['id_campus']) && !empty($_GET['id_campus'])){
        $id_campus=$_GET['id_campus'];
    }else{
       $id_campus=null; 
    }
    
    $aux_list_campus=$this->masthead_model->get_campus($id_campus);
    $list_campus=$aux_list_campus->result_array();
    
    print_r(json_encode($list_campus));
    
  }
  
  
  public function get_campus_by_nivel(){
    if(isset($_POST['id_nivel_interes']) && !empty($_POST['nivel_interes'])){
        $id_nivel_interes=$_POST['id_nivel_interes'];  
        $nivel_interes=$_POST['nivel_interes'];        
        $aux_list_campus=$this->masthead_model->get_campus_by_interes($id_nivel_interes,$nivel_interes);
        $list_campus=$aux_list_campus->result_array();
        
        print_r(json_encode($list_campus));
    }
    
  }
  
  
  
  
  
  
  public function get_nivel_interes_by_campus_from_wordpress(){
    if(isset($_POST['campus_id']) && !empty($_POST['campus_id'])){
        $id_campus=$_POST['campus_id'];
        $aux_list_campus=$this->masthead_model->get_nivel_interes_by_campus_from_wordpress($id_campus);
        $list_campus=$aux_list_campus->result_array();
            
        print_r(json_encode($list_campus));
    }
    
  }
  
  
  
  public function get_programa_interes_by_campus__and_nivel_from_wordpress(){
    if(isset($_POST['campus_id']) && !empty($_POST['campus_id']) && isset($_POST['area_academica']) && !empty($_POST['area_academica'])  ){
        $id_campus=$_POST['campus_id'];
        $nivel_interes=$_POST['area_academica'];
        $aux_list_campus=$this->masthead_model->get_programa_interes_by_nivel_interes_from_workpress($id_campus,$nivel_interes);
        $list_campus=$aux_list_campus->result_array();
            
        print_r(json_encode($list_campus));
    }
    
  }
  
  
  public function get_colonia_by_cp(){
    if(isset($_POST['cp']) && !empty($_POST['cp'])){
        $cp=$_POST['cp'];
        $aux_ciudades=$this->masthead_model->get_colonia_by_cp($cp);
        $ciudades=$aux_ciudades->result_array();
        print_r(json_encode($ciudades));
    }
    
  }
  
  
  
  public function nivel_interes(){
        
        if(isset($_POST['id_campus'])){
            $this->load->model('becas_model');
            $aux_nivel=$this->becas_model->get_nivel_interes($_POST['id_campus']);
            $nivel=$aux_nivel->result_array();
            
            print_r(json_encode($nivel));
        }else{
            echo base_url();
        }
    }
    
    public function programa_interes(){
        
        if(isset($_POST['id_campus'])){
            $this->load->model('becas_model');
            $aux_progrma=$this->becas_model->get_programa_interes($_POST['id_campus'],$_POST['id_nivel_interes']);
            $programa=$aux_progrma->result_array();
            print_r(json_encode($programa));
        }
    }
  
  
  
  
  function robot(){
        
    set_time_limit(0);
    
    
                
        $aux_cp_data=$this->masthead_model->get_cp();
        
        $cp_data=$aux_cp_data->result_array();
        
        //print_r($cp_data);
        
        foreach($cp_data as $result){
            
            //echo $result['id_aux_cp'];
        $id_cp=$result['id_aux_cp'];
        $cp=$result['cp'];
        $colonia=$result['colonia'];
        $colonia_url=urlencode($colonia);
    
            $url = "http://maps.googleapis.com/maps/api/geocode/json?address=$colonia_url&components=postal_code:$cp&sensor=false";
            
            $resp = file_get_contents($url);
            $json = json_decode($resp);
            
            
//            echo "<pre>";
//            print_r($json->results[0]->geometry);
//            echo "</pre>";
            
            $latitud=$json->results[0]->geometry->location->lat;
            $longitud=$json->results[0]->geometry->location->lng;
            
            
            $latitud_northeast=$json->results[0]->geometry->bounds->northeast->lat;
            $longitud_northeast=$json->results[0]->geometry->bounds->northeast->lng;
            
            $latitud_southwest=$json->results[0]->geometry->bounds->southwest->lat;
            $longitud_southwest=$json->results[0]->geometry->bounds->southwest->lng;
            
            echo $latitud;
            if(isset($json->results[0]->geometry->location->lat)){
                
                //echo "funciona el no existe jejejejee";
//                
//                echo "</br>latitud  ".$latitud;
//                echo "</br>longitud  ".$longitud;
//                echo "</br>latitud_northeast  ".$latitud_northeast;
//                echo "</br>longitud_northeast  ".$longitud_northeast;
//                echo "</br>latitud_southwest  ".$latitud_southwest;
//                echo "</br>longitud_southwest  ".$longitud_southwest;
                
                $this->masthead_model->insert_coordenadas($latitud,$longitud,$latitud_northeast,$longitud_northeast,$latitud_southwest,$longitud_southwest,$id_cp);
                $this->masthead_model->update_aux_cp_by_insert_coordenadas($id_cp);
                
                
            }else{
                echo "chnagossss se rompio el robot ;( </br>";
                echo $url;
            }
    //fin foreach
    }
    
    
    
    
  }


    function submit_landing() {
              $tutor_email=$_POST['tutor_email'];
              $nombre = $_POST['nombre'];
              $apellido_paterno = $_POST['apellido_paterno'];
              $email = $_POST['email'];
              $telefono = $_POST['telefono'];
              $id_campus = $_POST['campus'];
              $id_programa_interes = $_POST['programas'];
              $id_nivel_interes = $_POST['areas'];
              $id_periodo = $_POST['periodo'];
              $id_modalidad = $_POST['modalidad'];

           /*   if(isset($_POST['tutor_email'])){
                $tutor_email = $_POST['tutor_email'];
              }*/
              
              $origen = $_POST['origen'];
        $detalle_origen = $_POST['detalle_origen'];
              //$modalidad=$_POST['modalidad'];
              //$periodo=$_POST['periodo'];

                date_default_timezone_set('America/Mexico_City');
              
              $now   = new DateTime('NOW');
              $fecha_de_registro =  $now->format('d/m/Y H:i');

              $logname = realpath(dirname(__FILE__)).'/csv/log.csv';
              $filename = realpath(dirname(__FILE__)).'/csv/list.csv';
	      log_message('error','---filename=>'.$filename);
              $file_respaldo = realpath(dirname(__FILE__)).'/csv/respaldo.csv';

              if(!empty($nombre) && 
              !empty($apellido_paterno) &&
              !empty($email) &&
              !empty($telefono) &&
              !empty($id_campus) &&
              !empty($id_nivel_interes) &&
              !empty($origen) &&
              !empty($id_nivel_interes) &&
              !empty($id_periodo) &&
              !empty($id_modalidad) &&
              !empty($id_programa_interes))
              {

                $nombre=utf8_decode($nombre);
                $apellido_paterno=utf8_decode($apellido_paterno);
                $email=utf8_decode($email);
                
                
                if(!file_exists($filename)){
                  $file = fopen($filename, 'wb') or die("can't open file");
                  fwrite($file, '"NOMBRE","APELLIDO_PATERNO","APELLIDO_MATERNO","CORREO_ELECTRONICO","CORREO_ELECTRONICO2","TELEFONO1","TELEFONO2","TELEFONO3","CIUDAD","ESTADO","PAIS","CAMPUS","NIVEL_INTERES","MODALIDAD","PERIODO","PROGRAMA_DE_INTERES","COMENTARIOS","ORIGEN","DETALLE_DE_ORIGEN","FECHA_DE_REGISTRO","WEB_ID","ANALYTICS","PERMITIR_CORREO","REQUISITO_ESTUDIOS","REQUISITO_CERTIFICADO","REQUISITO_PRESUPUESTO","CUSTOM1","CUSTOM2","CUSTOM3","CUSTOM4","CUSTOM5","CUSTOM6","CUSTOM7","CUSTOM8","CUSTOM9","CUSTOM10"'.PHP_EOL);
                  fclose($file);
                }
                
                if(!file_exists($file_respaldo)){
                  $file = fopen($file_respaldo, 'wb') or die("can't open file");
                  fwrite($file, '"NOMBRE","APELLIDO_PATERNO","APELLIDO_MATERNO","CORREO_ELECTRONICO","CORREO_ELECTRONICO2","TELEFONO1","TELEFONO2","TELEFONO3","CIUDAD","ESTADO","PAIS","CAMPUS","NIVEL_INTERES","MODALIDAD","PERIODO","PROGRAMA_DE_INTERES","COMENTARIOS","ORIGEN","DETALLE_DE_ORIGEN","FECHA_DE_REGISTRO","WEB_ID","ANALYTICS","PERMITIR_CORREO","REQUISITO_ESTUDIOS","REQUISITO_CERTIFICADO","REQUISITO_PRESUPUESTO","CUSTOM1","CUSTOM2","CUSTOM3","CUSTOM4","CUSTOM5","CUSTOM6","CUSTOM7","CUSTOM8","CUSTOM9","CUSTOM10"'.PHP_EOL);
                  fclose($file);
                }
                
                 $csv = "\"{$nombre}\",\"{$apellido_paterno}\",\"\",\"{$email}\",\"\",\"{$telefono}\",\"\",\"\",\"\",\"\",\"\",\"{$id_campus}\",\"{$id_nivel_interes}\",\"{$id_modalidad}\",\"{$id_periodo}\",\"{$id_programa_interes}\",\"\",\"{$origen}\",\"{$detalle_origen}\",\"{$fecha_de_registro}\",\"\",\"UA-5964406-1\",\"\",\"\",\"\",\"\",\"{$tutor_email}\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"".PHP_EOL;
                
                $file = fopen($filename, 'ab');
                fwrite($file, $csv);
                fclose($file);
                
                $file = fopen($file_respaldo, 'ab');
                fwrite($file, $csv);
                fclose($file);
                
                
                $this->load->model('becas_model');
                $this->becas_model->insert_becas($id_campus,$id_nivel_interes,$id_programa_interes,$id_periodo,$id_modalidad,$nombre,$apellido_paterno,$email,$telefono,$origen,$detalle_origen,$tutor_email);
                
                
                
                
                  
              }else{
              
                $referrer = $_SERVER['HTTP_REFERER'];
                
                if(!file_exists($logname)){
                  $file = fopen($logname, 'wb') or die("can't open file");
                  fwrite($file, '"NOMBRE","APELLIDO_PATERNO","APELLIDO_MATERNO","CORREO_ELECTRONICO","CORREO_ELECTRONICO2","TELEFONO1","TELEFONO2","TELEFONO3","CIUDAD","ESTADO","PAIS","CAMPUS","NIVEL_INTERES","MODALIDAD","PERIODO","PROGRAMA_DE_INTERES","COMENTARIOS","ORIGEN","DETALLE_DE_ORIGEN","FECHA_DE_REGISTRO","WEB_ID","ANALYTICS","PERMITIR_CORREO","REQUISITO_ESTUDIOS","REQUISITO_CERTIFICADO","REQUISITO_PRESUPUESTO","CUSTOM1","CUSTOM2","CUSTOM3","CUSTOM4","CUSTOM5","CUSTOM6","CUSTOM7","CUSTOM8","CUSTOM9","CUSTOM10"'.PHP_EOL);
                  fclose($file);
                }

                  $csv = "\"{$nombre}\",\"{$apellido_paterno}\",\"\",\"{$email}\",\"\",\"{$telefono}\",\"\",\"\",\"\",\"\",\"\",\"{$id_campus}\",\"{$id_nivel_interes}\",\"{$id_modalidad}\",\"{$id_periodo}\",\"{$id_programa_interes}\",\"\",\"{$origen}\",\"{$detalle_origen}\",\"{$fecha_de_registro}\",\"\",\"UA-5964406-1\",\"\",\"\",\"\",\"\",\"{$tutor_email}\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"".PHP_EOL;
                
                $file = fopen($logname, 'ab');
                fwrite($file, $csv);
                fclose($file);
                
              }
  
  
  
    }
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
