<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <?php
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
    <style type='text/css'>
        body
        {
            font-family: Arial;
            font-size: 14px;
        }
        a {
            color: blue;
            text-decoration: none;
            font-size: 14px;
        }
        a:hover
        {
            text-decoration: underline;
        }
    </style>
</head>
<body>
<div>
    <a href='<?php echo site_url('main/landing')?>'>Landing</a> |
    <a href='<?php echo site_url('main/programas_interes')?>'>Programas de interes</a> |
    <a href='<?php echo site_url('main/nivel_interes')?>'>Nivel de interes</a> |
    <a href='<?php echo site_url('main/beneficios')?>'>Beneficios</a> |
    <a href='<?php echo site_url('main/areas_academicas')?>'>Areas Academicas</a> |
    <a href='<?php echo site_url('main/formulario')?>'>Formulario</a> |
    <a href='<?php echo site_url('main/detalle_origen')?>'>Detalle de origen</a> |
    <a href='<?php echo site_url('main/origen')?>'>Origen</a> |
    <h1>Area Academica</h1>

</div>
<div style='height:20px;'></div>
<div>
    <?php echo $output; ?>
</div>
</body>
</html>
