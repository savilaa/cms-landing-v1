<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->helper('url');
    } 
    
	public function index()
	{
		//$this->load->view('welcome_message');
		echo"dsdsdsds";

	}
    /*Obtener datos e mandar los datos a la vista*/
	public function landing(){
        $this->load->model('formulario_model');
        $aux_form=$this->formulario->get_campus();
        $form['campus']=$aux_form->result_array();
        $aux_form=$this->formulario->get_origen();
        $form['origen']=$aux_form->result_array();
        $aux_form=$this->formulario->get_detalle_origen();
        $form['detalle_origen']=$aux_form->result_array();
        $formulario¨=array();
        $formulario['formulario'] = $this->load->view('formulario.php',$form,TRUE);
       
        
		$data = new stdClass();
        $formulario['header']=$this->load->view('tpl.header2.php',null,TRUE);
        $formulario['footer']=$this->load->view('tpl.footer2.php',null,TRUE);
		$data->content = $this->load->view('view_landing.php',$formulario,TRUE);
		$this->load->view('html.tpl.php',$data);
	}


    public function form(){
        $this->load->model('formulario_model');
        $aux_form=$this->formulario->get_campus();
        $form['campus']=$aux_form->result_array();
        print_r($form);
        $data = new stdClass();
        $data->content = $this->load->view('formulario.php',$form,TRUE);
		$this->load->view('html.tpl.php',$data);
        
    }
    

    
    public function programa_interes(){
        
        if(isset($_POST['id_campus'])){
            $this->load->model('formulario');
            $aux_progrma=$this->formulario->get_programa_interes($_POST['id_campus'],$_POST['id_programa_interes']);
            $programa=$aux_progrma->result_array();
            print_r(json_encode($programa));
        }
    }


    public function nivel_interes(){
        print_r($_POST);
        if(isset($_POST['id_campus'])){
            $this->load->model('formulario');
            $aux_nivel=$this->formulario->get_nivel_interes($_POST['id_campus']);
            $nivel=$aux_nivel->result_array();
            //$nivel=$aux_form->result_array();

            print_r(json_encode($nivel));
        }else{
            echo base_url();
        }
    }



    function submit_landing() {


        $tutor_email="";
        $nombre = $_POST['nombre'];
        $apellido_paterno = $_POST['apellido_paterno'];
        $correo_electronico = $_POST['email'];
        $telefono1 = $_POST['telefono'];
        $campus = $_POST['campus'];
        $programa_de_interes = $_POST['programas'];
        $nivel_interes = $_POST['areas'];
        $programa = $_POST['programas'];


        if(isset($_POST['tutor_email'])){
            $tutor_email = $_POST['tutor_email'];
        }
        exit();
        $origen = $_POST['origen'];
        $horario = $_POST['horario'];
        //$modalidad=1;
        $modalidad=$_POST['modalidad'];
        $periodo=$_POST['periodo'];




        date_default_timezone_set('America/Mexico_City');

        $now   = new DateTime('NOW');
        $fecha_de_registro =  $now->format('d/m/Y H:i');


        $logname = realpath(dirname(__FILE__)).'/csv/log.csv';
        $filename = realpath(dirname(__FILE__)).'/csv/list.csv';
        $file_respaldo = realpath(dirname(__FILE__)).'/csv/respaldo.csv';


        if(!empty($nombre) &&
            !empty($apellido_paterno) &&
            !empty($correo_electronico) &&
            !empty($telefono1) &&
            !empty($campus) &&
            !empty($nivel_interes) &&
            !empty($modalidad) &&
            !empty($periodo) &&
            !empty($programa_de_interes))
        {

            if(!file_exists($filename)){
                $file = fopen($filename, 'wb') or die("can't open file");
                fwrite($file, '"NOMBRE","APELLIDO_PATERNO","APELLIDO_MATERNO","CORREO_ELECTRONICO","CORREO_ELECTRONICO2","TELEFONO1","TELEFONO2","TELEFONO3","CIUDAD","ESTADO","PAIS","CAMPUS","NIVEL_INTERES","MODALIDAD","PERIODO","PROGRAMA_DE_INTERES","COMENTARIOS","ORIGEN","DETALLE_DE_ORIGEN","FECHA_DE_REGISTRO","WEB_ID","ANALYTICS","PERMITIR_CORREO","REQUISITO_ESTUDIOS","REQUISITO_CERTIFICADO","REQUISITO_PRESUPUESTO","CUSTOM1","CUSTOM2","CUSTOM3","CUSTOM4","CUSTOM5","CUSTOM6","CUSTOM7","CUSTOM8","CUSTOM9","CUSTOM10"'.PHP_EOL);
                fclose($file);
            }

            if(!file_exists($file_respaldo)){
                $file = fopen($file_respaldo, 'wb') or die("can't open file");
                fwrite($file, '"NOMBRE","APELLIDO_PATERNO","APELLIDO_MATERNO","CORREO_ELECTRONICO","CORREO_ELECTRONICO2","TELEFONO1","TELEFONO2","TELEFONO3","CIUDAD","ESTADO","PAIS","CAMPUS","NIVEL_INTERES","MODALIDAD","PERIODO","PROGRAMA_DE_INTERES","COMENTARIOS","ORIGEN","DETALLE_DE_ORIGEN","FECHA_DE_REGISTRO","WEB_ID","ANALYTICS","PERMITIR_CORREO","REQUISITO_ESTUDIOS","REQUISITO_CERTIFICADO","REQUISITO_PRESUPUESTO","CUSTOM1","CUSTOM2","CUSTOM3","CUSTOM4","CUSTOM5","CUSTOM6","CUSTOM7","CUSTOM8","CUSTOM9","CUSTOM10"'.PHP_EOL);
                fclose($file);
            }

            $csv = "\"{$nombre}\",\"{$apellido_paterno}\",\"\",\"{$correo_electronico}\",\"\",\"{$telefono1}\",\"\",\"\",\"\",\"\",\"\",\"{$campus}\",\"{$nivel_interes}\",\"{$modalidad}\",\"{$periodo}\",\"{$programa_de_interes}\",\"\",\"11\",\"{$origen}\",\"{$fecha_de_registro}\",\"\",\"UA-5964406-1\",\"\",\"\",\"\",\"\",\"{$tutor_email}\",\"{$horario}\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"".PHP_EOL;

            $file = fopen($filename, 'ab');
            fwrite($file, $csv);
            fclose($file);

            $file = fopen($file_respaldo, 'ab');
            fwrite($file, $csv);
            fclose($file);


            $this->load->model('formulario');
            $this->formulario->insert_registros($campus,$nivel_interes,$programa_de_interes,$modalidad,$periodo,$nombre,$apellido_paterno,$correo_electronico,$telefono1,$origen,$tutor_email);





        }else{

            $referrer = $_SERVER['HTTP_REFERER'];

            if(!file_exists($logname)){
                $file = fopen($logname, 'wb') or die("can't open file");
                fwrite($file, '"NOMBRE","APELLIDO_PATERNO","APELLIDO_MATERNO","CORREO_ELECTRONICO","CORREO_ELECTRONICO2","TELEFONO1","TELEFONO2","TELEFONO3","CIUDAD","ESTADO","PAIS","CAMPUS","NIVEL_INTERES","MODALIDAD","PERIODO","PROGRAMA_DE_INTERES","COMENTARIOS","ORIGEN","DETALLE_DE_ORIGEN","FECHA_DE_REGISTRO","WEB_ID","ANALYTICS","PERMITIR_CORREO","REQUISITO_ESTUDIOS","REQUISITO_CERTIFICADO","REQUISITO_PRESUPUESTO","CUSTOM1","CUSTOM2","CUSTOM3","CUSTOM4","CUSTOM5","CUSTOM6","CUSTOM7","CUSTOM8","CUSTOM9","CUSTOM10"'.PHP_EOL);
                fclose($file);
            }

            $csv = "\"{$nombre}\",\"{$apellido_paterno}\",\"\",\"{$correo_electronico}\",\"\",\"{$telefono1}\",\"\",\"\",\"\",\"\",\"\",\"{$campus}\",\"{$nivel_interes}\",\"{$modalidad}\",\"{$periodo}\",\"{$programa_de_interes}\",\"\",\"11\",\"{$origen}\",\"{$fecha_de_registro}\",\"\",\"UA-5964406-1\",\"\",\"\",\"\",\"\",\"{$tutor_email}\",\"{$referrer}\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"".PHP_EOL;

            $file = fopen($logname, 'ab');
            fwrite($file, $csv);
            fclose($file);

        }



    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
