<div id="footer" class="pin-frame">
    <div class="content_container">
        <div id="text-2" class="widget_text">
            <div class="textwidget">
                <div class="content">
                    <div class="group">
                        <div class="list">
                            <h3>Conoce Tecmilenio</h3>
                            <ul>
                                <li><a href="http://www.tecmilenio.mx/nosotros/">¿Quiénes somos?</a></li>
                                <li><a href="http://pi.tecmilenio.mx/">Programas internacionales</a></li>
                                <li><a href="http://cienciasdelafelicidad.mx/">Instituto de Ciencias de la Felicidad</a></li>
                            </ul>
                        </div>

                        <div class="list">
                            <h3>Oferta educativa</h3>
                            <ul>
                                <li><a href="/preparatoria/">Prepa Tecmilenio</a></li>
                                <li><a href="/carrera-profesional/">Carreras profesionales</a></li>
                                <li><a href="/carrera-ejecutiva/">Carreras ejecutivas</a></li>
                                <li><a href="/posgrado/">Maestrías</a></li>
                                <li><a href="/educacion_continua/">Educación continua</a></li>
                                <li><a href="http://www.campusenlinea.com/">En línea</a></li>
                            </ul>
                        </div>

                        <div class="list">
                            <h3>Admisiones</h3>
                            <ul>
                                <li><a href="/proceso-de-admisiones/pasos/">Proceso de admisión</a></li>
                                <li><a href="/proceso-de-admisiones/periodos/">Periodos de ingreso</a></li>
                                <li><a href="/colegiaturas/costos/">Colegiaturas</a></li>
                                <li><a href="/apoyos-financieros/convenios/">Convenios</a></li>
                                <li><a href="http://inscripciones.tecmilenio-rd.com/">Feria de inscripciones</a></li>
                                <li><a href="/apoyos-financieros/becas/">Apoyos financieros</a></li>
                            </ul>
                        </div>

                        <div class="list">
                            <h3>Campus</h3>
                            <ul>
                                <li><a href="/campus/">Presencial</a></li>
                                <li><a href="http://www.campusenlinea.com/">En línea</a></li>
                            </ul>
                        </div>
                    </div><!-- END GROUP -->

                    <div class="group">
                        <div class="list">
                            <h3>Alumnos</h3>
                            <ul>
                                <li><a href="http://portal.tecmilenio.edu.mx/irj/portal">Servicios en línea</a></li>
                                <li><a href="http://www.tecmilenio.mx/servicios/">Portal de alumnos</a></li>
                                <li><a href="http://bbsistema.tecmilenio.edu.mx/">Blackboard</a></li>
                                <li><a href="tecmilenio.mx/mail/">Email</a></li>
                                <li><a href="http://tecmilenio.occ.com.mx/Bolsa_Trabajo">Bolsa de trabajo</a></li>
                                <li><a href="http://www.tecmilenio.edu.mx/calendarios.htm">Calendario</a></li>
                            </ul>
                        </div>

                        <div class="list">
                            <h3>Egresados</h3>
                            <ul>
                                <li><a href="http://www.tecmilenio.edu.mx/centro-de-empleabilidad.htm">Centro de empleabilidad</a></li>
                                <li><a href="#">Plan de desarrollo profesional</a></li>
                                <li><a href="http://www.tecmilenio.edu.mx/bolsadetrabajo/alumno/">Bolsa de trabajo</a></li>
                                <li><a href="#">Beneficios graduados</a></li>
                                <li><a href="http://www.tecmilenio.mx/vinculacion_empresas.htm">Vinculación con empresas</a></li>
                            </ul>
                        </div>

                        <div class="list">
                            <h3>Profesores</h3>
                            <ul>
                                <li><a href="#">Congreso de maestros</a></li>
                            </ul>
                        </div>

                        <div class="list bold">
                            <ul>
                                <li><a href="#">Padres</a></li>
                                <li><a href="#">Noticias</a></li>
                                <li><a href="#">Blog</a></li>
                            </ul>
                        </div>
                    </div><!-- END GROUP -->

                    <div class="group">
                        <div class="list">
                            <h3>Contáctanos</h3>
                            <ul>
                                <li><a href="/contactanos/">Contacto</a></li>
                                <li><a href="/campus/">Directorio de campus</a></li>
                                <li><a href="#">¿Quieres ser profesor?</a></li>
                                <li><a href="#">FAQ’s</a></li>
                            </ul>
                        </div>
                    </div><!-- END GROUP -->
                </div>
            </div>
        </div>
    </div>
    <div class="legal">
        <p>
            <strong>UNIVERSIDAD TECMILENIO</strong> D.R.© Enseñanza e Investigación Superior, A.C. Monterrey, N.L. México. 2012. <a href="/aviso-de-privacidad/">Aviso</a> y <a href="/politicas-de-privacidad/">Política de Privacidad</a>.
            <a class="mapa" href="#"><span>&#9650</span> Mapa de sitio</a>
        </p>
    </div>
</div><!-- END #FOOTER -->

<div id="footer2">
    <div class="legal2">
        <p>
            <strong>UNIVERSIDAD TECMILENIO</strong> D.R.© Enseñanza e Investigación Superior, A.C. Monterrey, N.L. México. 2012. <a href="/aviso-de-privacidad/">Aviso</a> y <a href="/politicas-de-privacidad/">Política de Privacidad</a>.
            <a class="mapa" href="#"><span>&#9650</span> Mapa de sitio</a>
        </p>
    </div>
</div><!-- END #FOOTER -->

<style>
    #footer .legal p strong .footer2{
        color: #4DB844 !important;
        text-transform: uppercase;
    }
</style>
<script type='text/javascript' src='http://tecmilenio.mx/wp-includes/js/jquery/jquery.form.min.js?ver=2.73'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var myAjax = {"ajaxurl":"http:\/\/tecmilenio.mx\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>

<script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/wp-views/embedded/res/js/wpv-pagination-embedded.js?ver=1.2.2'></script>
<script>
    jQuery(document).ready(function($){
        //$('.texto').mCustomScrollbar();
        $('.horario label').click(function(){
            $('#horario').show();
        });

        $('#opciones li').click(function(){
            $('#opciones li').removeClass('active');
            $(this).addClass('active');
            var index = $('#opciones li').index(this);
            $('.texto').hide();
            $('.texto').eq(index).show();
        });

    });
</script>

<script src="<?=base_url()?>assets/js/placeholders.min.js"></script>

<script src="<?=base_url()?>assets/universal/js/idangerous.swiper-2.1.min.js"></script>
<!--<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.validate.min.js"></script>

    <script type="text/javascript" charset="utf-8">
    $(window).load(function() {
        $('.flexslider').flexslider({
                controlsContainer: '.flex-container'
            });
        });
    </script>-->

<script>
    var mySwiper = new Swiper('.swiper-container',{
    pagination: '.pagination',
        loop:true,
        grabCursor: true,
        paginationClickable: true
    })
    $('.arrow-left').on('click', function(e){
        e.preventDefault()
        mySwiper.swipePrev()
    })
    $('.arrow-right').on('click', function(e){
        e.preventDefault()
        mySwiper.swipeNext()
    })
        </script>

<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/greensock/TweenMax.min.js'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/greensock/plugins/ScrollToPlugin.min.js'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery.superscrollorama.js'></script>
<!--  <script type='text/javascript' src="<?php echo base_url()?>assets/js/home.js"></script>
<script src="<?=base_url()?>assets/js/script.js"></script> -->
<script>
    $(function(){
        $('a[href*=#]').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
            && location.hostname == this.hostname) {
                var $target = $(this.hash);
                $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
                if ($target.length) {
                    var targetOffset = $target.offset().top;
                    $('html,body').animate({scrollTop: targetOffset}, 500);
                    return false;
                }}
        });
    });
</script>
<script>
    $('#footer .legal p .mapa').click(function(){
        if($('#footer .content_container').height() <= 45){
            $(this).html('<span>&#9660</span> Mapa de sitio');
            TweenLite.to($('#footer .content_container'), .25, {css:{height: 385}, onComplete: function(){ }, ease:Quad.easeOut});
        }else{
            $(this).html('<span>&#9650</span> Mapa de sitio');
            TweenLite.to($('#footer .content_container'), .25, {css:{height: 45}, onComplete: function(){ }, ease:Quad.easeOut});
        }
        return false;
    });
</script>




<script type="text/javascript">
    var Comm100API=Comm100API||{chat_buttons:[]};
    Comm100API.chat_buttons.push({code_plan:25,div_id:'comm100-button-25'});
    Comm100API.site_id=160524;
    Comm100API.main_code_plan=25;
    (function(){
        var lc=document.createElement('script');
        lc.type='text/javascript';lc.async=true;
        lc.src='https://chatserver.comm100.com/livechat.ashx?siteId='+Comm100API.site_id;
        var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(lc,s);
    })();
</script>
<!--End Comm100 Live Chat Code-->

</body>
</html>