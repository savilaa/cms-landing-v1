<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Main extends CI_Controller {


    function main()
    {
        parent::__construct();
 
        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */ 
 
        $this->load->library('grocery_CRUD');
 
    }
 
    public function index()
    {
        echo "<h1>Welcome to the world of Codeigniter</h1>";//Just an example to ensure that we get into the function
        die();
    }

    public function landing()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('landing');
        $crud->set_relation('id_nivel_interes','nivel_interes','nivel_interes');
        $crud->set_relation('id_origen','origen','origen');
        $crud->set_relation('id_detalle_origen','detalle_origen','detalle_origen');
        $crud->set_relation('id_formulario','formulario','formulario_nombre');
        $crud->set_relation('id_area_academica','areas_academicas','area_academica');
        $crud->set_subject('Landing');
        $crud->fields('url','titulo','frase','img_principal','beneficios_titulo','video','status','codigo_conversion','label','id_formulario','id_nivel_interes','id_origen','id_detalle_origen','descripcion','id_area_academica');
        $crud->set_field_upload('img_principal','assets/uploads/files');
        $crud->display_as('id_nivel_interes','Nivel de   Interes')->display_as('img_principal','Imagen Principal')->display_as('id_formulario','Formulario')->display_as('id_origen','Origen')->display_as('id_detalle_origen','Detalla de Orige');
        $crud->required_fields('url','titulo','frase','img_principal','beneficios_titulo','video','status','codigo_conversion','label','id_nivel_interes','id_origen','id_detalle_origen','id_formulario');




        $output = $crud->render();
        $this->_example_landing($output);

    }

    function _example_landing($output = null)

    {
        $this->load->view('landing.php',$output);

    }
    public function programas_interes()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('programa_interes');
        $crud->set_relation('id_area_academica','areas_academicas','area_academica');
        $crud->set_relation('id_nivel_interes','nivel_interes','nivel_interes');
        $crud->set_subject('Programa Interes');
        $crud->fields('clave_programa_interes','programa_interes','url_programa_interes','status','id_nivel_interes','id_area_academica');
        $crud->display_as('id_nivel_interes','Nivel de   Interes')->display_as('id_area_academica','Area Acdemica')->display_as('url_programa_interes','URL');
        $crud->required_fields('clave_programa_interes','programa_interes','url_programa_interes','status','id_nivel_interes','id_area_academica');


        $output = $crud->render();
        $this->_example_programas_interes($output);
    }

    function _example_programas_interes($output = null)

    {
        $this->load->view('programa_interes.php',$output);
    }
    public function nivel_interes()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('nivel_interes');

        $crud->set_subject('Nivel de Interes');
        $crud->fields('clave_nivel_interes','nivel_interes','status');
        $crud->required_fields('clave_nivel_interes','nivel_interes','status');


        $output = $crud->render();

        $this->_example_nivel_interes($output);
    }

    function _example_nivel_interes($output = null)

    {
        $this->load->view('nivel_interes.php',$output);
    }
    public function beneficios()
    {

        $crud = new grocery_CRUD();
        $crud->set_table('beneficios');
           $crud->set_subject('beneficios');
        $crud->fields('imagen','titulo','content','valor','status','id_nivel_interes');
        $crud->required_fields('imagen','titulo','content','valor','status','id_nives_interes');
        $crud->set_field_upload('imagen','assets/uploads/files');
        $crud->set_relation('id_nivel_interes','nivel_interes','nivel_interes');
        $crud->unset_texteditor('content','full_text');

        $output = $crud->render();

        $this->_example_beneficios($output);
    }

    function _example_beneficios($output = null)

    {
        $this->load->view('beneficios.php',$output);
    }
    public function origen()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('origen');
        $crud->set_subject('Origen');
        $crud->fields('clave_origen','origen','status');
        $crud->required_fields('clave_origen','origen','status');

        $output = $crud->render();

        $this->_example_origen($output);
    }

    function _example_origen($output = null)

    {
        $this->load->view('origen.php',$output);
    }
    public function detalle_origen()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('detalle_origen');
        $crud->set_subject('Detalle de Origen');
        $crud->fields('clave_detalle_origen','detalle_origen','status');
        $crud->required_fields('clave_detalle_origen','detalle_origen','status');
        $output = $crud->render();

        $this->_example_detalle_origen($output);
    }

    function _example_detalle_origen($output = null)

    {
        $this->load->view('detalle_origen.php',$output);
    }
    public function formulario()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('formulario');
        $crud->set_subject('Formulario');
        $crud->fields('formulario_nombre','status');
        $crud->required_fields('formulario_nombre','status');
         $crud->set_field_upload('formulario_nombre','assets/uploads/files');
        $output = $crud->render();

        $this->_example_formulario($output);
    }

    function _example_formulario($output = null)

    {
        $this->load->view('formulario.php',$output);
    }
    public function areas_academicas()
            {


        $crud = new grocery_CRUD();

        $crud->set_table('areas_academicas')
            ->set_subject('Areas Academicas')
            ->columns('area_academica','imagen_area_academica','status','id_nivel_interes');

        $crud->display_as('id_nivel_interes','Nivel Interes');

        $crud->fields('area_academica','imagen_area_academica','status','id_nivel_interes');
        $crud->required_fields('area_academica','imagen_area_academica','status','id_nivel_interes');
        $crud->set_field_upload('imagen_area_academica','assets/uploads/files');
                $crud->set_relation('id_nivel_interes','nivel_interes','nivel_interes');

        $output = $crud->render();



       // $output = $this->grocery_crud->render($outputs);
        $this->_example_areas_academicas($output);
    }

    function _example_areas_academicas($output = null)
    {
        $this->load->view('areas_academicas.php',$output);
    }

        function test()
        {
        $this->load->helper('url');
        $dato=uri_string();
            $this->load->model('landing_model','landing');
            $landing=$this->landing->existe_url($dato);

    if($landing=!null){


     /*   $a = modelo->metodo($path);

        $this->load->model('landing_model','landing');
        $registros=$this->landing->obtener_registros($landing);
        $this->armaLanding($registros);

    }else{
       $this->load->view('error.php');
    }
        //$a = modelo->metodo($path);


        // $a debe ser un query con los datos de las relaciones necesarias para cargar sus datos (JOINS)
        
            validar si existe el lp
                si, paso a controlador b info para recuperar datos
                no, mando a vista de no encontrado

        */

    /*
    metodo : armaLanding
    recupera datos para mandar a template y mostrar. 
    vars : obj #datos_landig
    autor : chava, anel
    */
        }
    public function armaLanding($landing)
    {
        /*
            ir por la vista del formulario que le toca, por su tipo $a->tipo_de_formulario
            determinar el tipo de zona verde
                si tiene desc, la paso y no pelo la oferta
                si no tiene desc, recupero sus areas academicas con $a->id_nivel
                    llamo metodo para armar oferta
                recupero los beneficios con $a->id_nivel
                crear y asignar nueva variable para pasar todos los datos a vista
                    $datos->landing = $a;
                    $datos->zona_verde->tipo = "desc"
                    $datos->zona_verde->datos = $variable_con_oferta_agrupada_por_nivel
                cargo en variable template de formulario que le toca con $a->id_tipo
                    $datos->formulario = $this->load->view($)
                paso datos a vista
                cargo vista
        */

        /*
        return : obj | false si no encuentra
        */
    }

    /*
    metodo : armaLanding
    recupera datos para mandar a template y mostrar. 
    vars : obj #datos_landig
    autor : chava, anel
    */
   private function seleccionaTipoBarraVerde($ofertas_por_nivel)
   {

        /*
            recupero la oferta por cada nivel con $id_nivel->id y lo guardo
                    $variable_con_oferta_agrupada_por_nivel[0] = $ofertadetipo0
                        $variable_con_oferta_agrupada_por_nivel[0]['titulo']
                        $variable_con_oferta_agrupada_por_nivel[0]['imagen']
                        $variable_con_oferta_agrupada_por_nivel[0]['og¿ferta'] = array();
                    $variable_con_oferta_agrupada_por_nivel[1] = $ofertadetipo1
        */
        return $variable_con_oferta;
    }
}
 
/* End of file main.php */
/* Location: ./application/controllers/main.p
