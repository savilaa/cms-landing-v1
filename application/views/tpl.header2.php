<body>
<!-- menu - header -->
<div class="header-container">
    <header class="header clear" role="banner">
        <!-- logo -->
        <h1 class="logo">
            <a href="http://tecmilenio.mx"><img src="http://tecmilenio.mx/wp-content/themes/tecmilenio/img/universidad-tecmilenio.gif" ></a>
        </h1>
        <!-- /logo
        <a target="_blank" href="http://itesm.mx/" class="link_tec">TECNOLÓGICO DE MONTERREY</a>-->
        <!-- secondary nav -->
        <div id="secondary-menu-container">
            <ul id="menu-menu-secundario" class="menu">
                <li id="menu-item-1057" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1057"><a href="#">Alumnos</a>
                    <ul class="sub-menu">
                        <li id="menu-item-1060" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1060"><a href="http://portal.tecmilenio.edu.mx/irj/portal">Servicio en línea</a></li>
                        <li id="menu-item-1061" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1061"><a href="http://www.tecmilenio.edu.mx/servicios/">Servicios para alumnos</a></li>
                        <li id="menu-item-1062" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1062"><a href="http://bbsistema.tecmilenio.edu.mx/">Blackboard</a></li>
                        <li id="menu-item-1063" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1063"><a href="http://prueba.http://tecmilenio.mx.php53-10.ord1-1.websitetestlink.com//mail/">Email</a></li>
                        <li id="menu-item-1065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1065"><a href="http://http://tecmilenio.mx/pi">Programas Internacionales</a></li>
                        <li id="menu-item-1066" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1066"><a href="http://cienciasdelafelicidad.mx/">Instituto de la Felicidad</a></li>
                        <li id="menu-item-1072" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1072"><a href="/calendarios">Calendario</a></li>
                        <li id="menu-item-4326" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4326"><a href="http://tecmilenio.occ.com.mx/">Bolsa de Trabajo</a></li>
                    </ul>
                </li>
                <li id="menu-item-4327" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4327"><a href="#">Graduados</a>
                    <ul class="sub-menu">
                        <li id="menu-item-4328" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4328"><a href="http://graduados.http://tecmilenio.mx/">Portal de Graduados</a></li>
                        <li id="menu-item-4329" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4329"><a href="http://tecmilenio.occ.com.mx/Bolsa_Trabajo">Bolsa de Trabajo</a></li>
                    </ul>
                </li>
                <li id="menu-item-4330" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4330"><a href="#">Entérate</a>
                    <ul class="sub-menu">
                        <li id="menu-item-4331" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4331"><a href="http://http://tecmilenio.mx/blog/">Blog</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- nav -->
        <nav class="nav" role="navigation">
            <ul>
                <li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14">
                    <div class="arrow" style="">
                    </div>
                    <a href="#">Conoce Tecmilenio</a>
                    <ul class="conoce-tec sub-menu">
                        <li id="menu-item-image" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-image">
                            <img src="http://tecmilenio.mx/wp-content/themes/tecmilenio/img/quienesomos.png" width="209" height="143" />
                        </li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-content">
                            <ul class="conoce-tec-content-titles">
                                <li>Universidad Tecmilenio
                                    <ul class="conoce-tec-content">
                                        <li class="mensaje-del-rector">
                                            <a href="/mensajes-del-rector/">Mensaje del rector</a>
                                        </li>
                                        <li class="quienes-somos-">
                                            <a href="/nosotros/">¿Quiénes Somos? </a>
                                        </li>
                                        <li class="videoteca">
                                            <a href="/videoteca/">Videoteca</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>Modelo Educativo
                                    <ul class="conoce-tec-content">
                                        <li class="nuevo-modelo-educativo">
                                            <a href="/modelo-de-universidad">Nuevo Modelo Educativo</a>
                                        </li>
                                        <li class="que-opinan-los-expertos">
                                            <a href="/expertos/">¿Qué opinan los expertos?</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>Conoce Más
                                    <ul class="conoce-tec-content">
                                        <li class="instituto-de-ciencias-de-la-felicidad">
                                            <a href="http://cienciasdelafelicidad.mx/">Instituto de Ciencias de la Felicidad</a>
                                        </li>
                                        <li class="programas-internacionales">
                                            <a href="/pi/">Programas Internacionales</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                </li>
                </li>
                </li>
                </li>
                </li>
                </li>
                </li>
                </li>
                </li>
                </li>
                <li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15">
                    <div class="arrow" style=""></div>
                    <a href="#">Oferta Educativa</a>
                    <ul class="oferta-ed sub-menu">
                        <li id="menu-item-image" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-image">
                            <img src="http://tecmilenio.mx/wp-content/themes/tecmilenio/img/carrerasprofe.png" width="209" height="143" />
                        </li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-content">
                            <ul class="oferta-content">
                                <li id="menu-item-16" class="prepa-tecmilenio menu-item menu-item-type-custom menu-item-object-custom menu-item-16">
                                    <a href="/preparatoria/">Prepa Tecmilenio</a>
                                </li>
                                <li id="menu-item-17" class="carreras-profesionales menu-item menu-item-type-custom menu-item-object-custom menu-item-17">
                                    <a href="/carrera-profesional/">Carreras Profesionales</a></li><li id="menu-item-18" class="carreras-ejecutivas menu-item menu-item-type-custom menu-item-object-custom menu-item-18"><a href="/carrera-ejecutiva/">Carreras Ejecutivas</a></li><li id="menu-item-19" class="maestrias menu-item menu-item-type-custom menu-item-object-custom menu-item-19"><a href="/posgrado/">Maestrías</a></li><li id="menu-item-1050" class="en-linea menu-item menu-item-type-custom menu-item-object-custom menu-item-1050"><a href="http://www.campusenlinea.com/">En Línea</a></li><li id="menu-item-1472" class="certificados menu-item menu-item-type-custom menu-item-object-custom menu-item-1472"><a href="/certificados-psicologia-positiva/certificado/">Certificados</a></li>  </ul>
                        </li>
                    </ul></li></li></li></li></li></li></li><li id="menu-item-20" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-20">
                    <div class="arrow" style=""></div>
                    <a href="/campus/">Campus</a><ul class="campus sub-menu"><li id="menu-item-image" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-image"><img src="http://tecmilenio.mx/wp-content/uploads/2013/08/campus.png" width="209" height="143" /></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-content">
                            <ul class="campus-content"><li id="menu-item-1475" class="cancun menu-item menu-item-type-custom menu-item-object-custom menu-item-1475"><a href="/campus/universidad-cancun-quintana-roo/">Cancún</a></li><li id="menu-item-1477" class="chihuahua menu-item menu-item-type-custom menu-item-object-custom menu-item-1477"><a href="/campus/universidad-chihuahua/">Chihuahua</a></li><li id="menu-item-1476" class="ciudad-juarez menu-item menu-item-type-custom menu-item-object-custom menu-item-1476"><a href="/universidad-ciudad-juarez-chihuahua/">Ciudad Juarez</a></li><li id="menu-item-1478" class="ciudad-obregon menu-item menu-item-type-custom menu-item-object-custom menu-item-1478"><a href="/campus/universidad-ciudad-obregon-sonora/">Ciudad Obregon</a></li><li id="menu-item-1479" class="cuautitlan menu-item menu-item-type-custom menu-item-object-custom menu-item-1479"><a href="/campus/universidad-cuautitlan-izcalli-estado-de-mexico/">Cuautitlan</a></li><li id="menu-item-1480" class="cuernavaca menu-item menu-item-type-custom menu-item-object-custom menu-item-1480"><a href="/campus/universidad-cuernavaca-morelos/">Cuernavaca</a></li><li id="menu-item-1481" class="culiacan menu-item menu-item-type-custom menu-item-object-custom menu-item-1481"><a href="/campus/universidad-culiacan-sinaloa/">Culiacan</a></li><li id="menu-item-1482" class="cumbres menu-item menu-item-type-custom menu-item-object-custom menu-item-1482"><a href="/campus/universidad-cumbres-monterrey-nuevo-leon/">Cumbres</a></li><li id="menu-item-1483" class="durango menu-item menu-item-type-custom menu-item-object-custom menu-item-1483"><a href="/campus/universidad-durango/">Durango</a></li><li id="menu-item-1484" class="ferreria menu-item menu-item-type-custom menu-item-object-custom menu-item-1484"><a href="/campus/universidad-ferreria-mexico-df/">Ferreria</a></li><li id="menu-item-1485" class="guadalajara menu-item menu-item-type-custom menu-item-object-custom menu-item-1485"><a href="/campus/universidad-guadalajara-jalisco/">Guadalajara</a></li><li id="menu-item-1486" class="guadalupe menu-item menu-item-type-custom menu-item-object-custom menu-item-1486"><a href="/campus/universidad-guadalupe-monterrey-nuevo-leon/">Guadalupe</a></li><li id="menu-item-1488" class="hermosillo menu-item menu-item-type-custom menu-item-object-custom menu-item-1488"><a href="/campus/universidad-hermosillo-sonora/">Hermosillo</a></li><li id="menu-item-1489" class="laguna menu-item menu-item-type-custom menu-item-object-custom menu-item-1489"><a href="/campus/universidad-laguna-torreon-coahuila/">Laguna</a></li><li id="menu-item-1490" class="las-torres menu-item menu-item-type-custom menu-item-object-custom menu-item-1490"><a href="/universidad-las-torres-monterrey-nuevo-leon/">Las Torres</a></li><li id="menu-item-1491" class="los-mochis menu-item menu-item-type-custom menu-item-object-custom menu-item-1491"><a href="/campus/universidad-los-mochis/">Los Mochis</a></li><li id="menu-item-1492" class="matamoros menu-item menu-item-type-custom menu-item-object-custom menu-item-1492"><a href="/campus/universidad-matamoros-tamaulipas/">Matamoros</a></li><li id="menu-item-1493" class="mazatlan menu-item menu-item-type-custom menu-item-object-custom menu-item-1493"><a href="/campus/universidad-mazatlan-sinaloa/">Mazatlán</a></li><li id="menu-item-1494" class="merida menu-item menu-item-type-custom menu-item-object-custom menu-item-1494"><a href="/campus/universidad-merida-yucatan/">Mérida</a></li><li id="menu-item-1495" class="morelia menu-item menu-item-type-custom menu-item-object-custom menu-item-1495"><a href="/campus/universidad-morelia-michoacan/">Morelia</a></li><li id="menu-item-1496" class="nuevo-laredo menu-item menu-item-type-custom menu-item-object-custom menu-item-1496"><a href="/campus/universidad-nuevo-laredo-tamaulipas/">Nuevo Laredo</a></li><li id="menu-item-1497" class="puebla menu-item menu-item-type-custom menu-item-object-custom menu-item-1497"><a href="/campus/universidad-puebla-puebla/">Puebla</a></li><li id="menu-item-1498" class="queretaro menu-item menu-item-type-custom menu-item-object-custom menu-item-1498"><a href="/campus/universidad-queretaro/">Querétaro</a></li><li id="menu-item-1499" class="reynosa menu-item menu-item-type-custom menu-item-object-custom menu-item-1499"><a href="/campus/universidad-reynosa-tamaulipas/">Reynosa</a></li><li id="menu-item-1524" class="san-luis-potosi menu-item menu-item-type-custom menu-item-object-custom menu-item-1524"><a href="/campus/universidad-san-luis-potosi/">San Luis Potosí</a></li><li id="menu-item-1525" class="san-nicolas menu-item menu-item-type-custom menu-item-object-custom menu-item-1525"><a href="/campus/universidad-san-nicolas-monterrey-nuevo-leon/">San Nicolás</a></li><li id="menu-item-1526" class="toluca menu-item menu-item-type-custom menu-item-object-custom menu-item-1526"><a href="/campus/universidad-toluca-estado-de-mexico/">Toluca</a></li><li id="menu-item-1527" class="veracruz menu-item menu-item-type-custom menu-item-object-custom menu-item-1527"><a href="/campus/universidad-veracruz-jalapa/">Veracruz</a></li><li id="menu-item-1528" class="villahermosa menu-item menu-item-type-custom menu-item-object-custom menu-item-1528"><a href="/campus/universidad-villahermosa-tabasco/">Villahermosa</a></li><li id="menu-item-1529" class="zapopan menu-item menu-item-type-custom menu-item-object-custom menu-item-1529"><a href="/campus/universidad-zapopan-jalisco/">Zapopan</a></li><li id="menu-item-4293" class="en-linea menu-item menu-item-type-custom menu-item-object-custom menu-item-4293"><a href="/campus/en-linea/">En línea</a></li>  </ul>
                        </li>
                    </ul></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li><li id="menu-item-21" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-21">
                    <div class="arrow" style=""></div>
                    <a href="#">Admisiones</a><ul class="proceso-ad sub-menu"><li id="menu-item-image" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-image"><img src="http://tecmilenio.mx/wp-content/themes/tecmilenio/img/ProcesodeAdm.png"/></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-content">
                            <ul class="proceso-ad-content-titles"><li>Proceso de admisión
                                    <ul class="proceso-ad-content"><li><a href="/proceso-de-admisiones/pasos/">Pasos</a></li><li><a href="/proceso-de-admisiones/requisitos/">Requisitos</a></li><li><a href="/proceso-de-admisiones/periodos/">Periodos de Ingreso</a></li><li><a href="http://inscripciones.tecmilenio-rd.com/">Feria de Inscripciones</a></li>	</ul>
                                </li><li>Colegiatura
                                    <ul class="proceso-ad-content"><li><a href="/colegiaturas/costos/">Colegiatura</a></li><li><a href="/colegiaturas/pagos/">Pagos</a></li><li><a href="/colegiaturas/seguros/">Seguros</a></li>	</ul>
                                </li><li>Apoyos Financieros
                                    <ul class="proceso-ad-content"><li><a href="/apoyos-financieros/becas/tipos-de-becas/">Becas</a></li><li><a href="/apoyos-financieros/credito-educativo/caracteristicas/">Crédito Educativo</a></li><li><a href="/apoyos-financieros/plan-de-educacion-a-futuro/que-es-pef/">Plan de Educación a Futuro</a></li><li><a href="/apoyos-financieros/financiamiento-lumni/informacion-general-lumni/">Financiamiento LUMNI</a></li><li><a href="/apoyos-financieros/convenios/">Convenios</a></li>	</ul>
                                </li>	</ul>
                        </li>
                    </ul></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li></li><li id="menu-item-4325" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4325">
                    <div class="arrow" style=""></div>
                    <a href="/contacto/contactanos">Contacto</a><ul class="contacto-ad sub-menu"><li id="menu-item-image" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-image"><img src="http://tecmilenio.mx/wp-content/themes/tecmilenio/img/Directorio-de-Campus.jpg"/></li><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-content">
                            <ul class="contacto-ad-content"><li id="menu-item-4503" class="inicia-tu-solicitud menu-item menu-item-type-custom menu-item-object-custom menu-item-4503"><a href="/contacto/contactanos/">Inicia tu solicitud</a></li><li id="menu-item-4504" class="directorio-campus menu-item menu-item-type-custom menu-item-object-custom menu-item-4504"><a href="/contacto/directorio-de-campus/">Directorio Campus</a></li><li id="menu-item-4502" class="unete-a-nosotros menu-item menu-item-type-custom menu-item-object-custom menu-item-4502"><a href="https://career8.successfactors.com/career?career_company=ITESM&lang=es_MX&company=ITESM&site=&loginFlowRequired=true">Únete a nosotros</a></li>	</ul>
                        </li>
                    </ul></li></li></li></li></ul>
        </nav>
        <!-- /nav -->
        <!-- search -->
        <form id="header-search" class="search" method="get" action="http://tecmilenio.mx" role="search">
            <input class="search-input" type="search" name="s" placeholder="Buscar">
        </form>
        <!-- /search -->
    </header>
</div>
<!--menu móvil-->
<div class="rp-mobile">
<div id="menu-rp">
    <div class="bars-menu-rp"></div>
    <div class="bars-menu-rp"></div>
    <div class="bars-menu-rp"></div>
    <div class="bars-menu-rp"></div>
</div>
<img src="http://tecmilenio.mx/wp-content/themes/tecmilenio/img/universidad-tecmilenio.gif" class="logo-rp">
<!--dropdwon primer nivel-->
<div id="menu-dropdown" class="hide-menu">
    <div id="scroll-menu">
        <div class="list-part" id="conoce">
            <div class="text-content conoce">
                <p class="blue-responsive">Conoce Tecmilenio</p>
            </div>
        </div>
        <div class="list-part" id="oferta">
            <div class="text-content oferta">
                <p class="blue-responsive">Oferta educativa</p>
            </div>
        </div>
        <div class="list-part">
            <div class="text-content campusf">
                <p class="blue-responsive">Campus</p>
            </div>
        </div>
        <div class="list-part" id="admisiones">
            <div class="text-content admisiones">
                <p class="blue-responsive">Admisiones</p>
            </div>
        </div>
        <a href="http://tecmilenio.mx/contacto/contactanos/">
            <div class="list-part" id="contacto">
                <div class="text-content contacto">
                    <p class="blue-responsive">Contáctanos</p>
                </div>
            </div>
        </a>
        <div class="list-part" id="alumnos">
            <div class="text-content alumnos">
                <p class="green-responsive">Alumnos</p>
            </div>
        </div>
        <div class="list-part" id="egresados">
            <div class="text-content egresados">
                <p class="green-responsive">Graduados</p>
            </div>
        </div>
        <a href="http://tecmilenio.mx/blog/">
            <div class="list-part" id="profesores">
                <div class="text-content profesores">
                    <p class="green-responsive">Entérate</p>
                </div>
            </div>
        </a>
    </div>
</div>
<!---->
<!--dropdown segundo nivel-->
<div id="conoce-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
        </div>
    </div>
    <div class="list-part" id="conoce2">
        <div class="text-content arrow-select">
            <p><span></span>Universidad Tecmilenio</p>
        </div>
    </div>
    <div class="list-part" id="conoce3">
        <div class="text-content arrow-select">
            <p><span></span>Modelo educativo</p>
        </div>
    </div>
    <div class="list-part" id="conoce4">
        <div class="text-content arrow-select">
            <p><span></span>Conoce más</p>
        </div>
    </div>
</div>
<div id="oferta-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
        </div>
    </div>
    <a href="http://tecmilenio.mx/preparatoria/">
        <div class="list-part">
            <div class="text-content arrow-select">
                <p><span></span>Prepa Tecmilenio</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/carrera-profesional/">
        <div class="list-part">
            <div class="text-content arrow-select">
                <p><span></span>Carreras Profesionales</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/carrera-ejecutiva/">
        <div class="list-part">
            <div class="text-content arrow-select">
                <p><span></span>Carreras Ejecutivas</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/posgrado/">
        <div class="list-part"  >
            <div class="text-content arrow-select">
                <p><span></span>Maestrías</p>
            </div>
        </div>
    </a>
    <a href="http://www.campusenlinea.com/">
        <div class="list-part">
            <div class="text-content arrow-select">
                <p><span></span>En Línea</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/certificados-psicologia-positiva/certificado/">
        <div class="list-part"  >
            <div class="text-content arrow-select">
                <p><span></span>Certificados</p>
            </div>
        </div>
    </a>
</div>
<div id="campus-dropdown" class="hide-menu">
<div class="sub-nav">
    <div class="back-last back">
        <p class="back-item">Menu principal</p>
    </div>
</div>
<div id="scroll-menu" style="height:85% !important;">
<a href="http://tecmilenio.mx/campus/universidad-cancun-quintana-roo/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Cancún</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-chihuahua/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Chihuahua</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-ciudad-juarez-chihuahua/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Ciudad Juarez</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-ciudad-obregon-sonora/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Ciudad Obregon</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-cuautitlan-izcalli-estado-de-mexico/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Cuautitlan</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-cuernavaca-morelos/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Cuernavaca</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-culiacan-sinaloa/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Culiacan</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-cumbres-monterrey-nuevo-leon/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Cumbres</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-durango/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Durango</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-ferreria-mexico-df/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Ferreria</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-guadalajara-jalisco/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Guadalajara</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-guadalupe-monterrey-nuevo-leon/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Guadalupe</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-hermosillo-sonora/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Hermosillo</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-laguna-torreon-coahuila/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Laguna</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-las-torres-monterrey-nuevo-leon/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Las Torres</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-los-mochis-sinaloa/">
    <div class="list-part" >
        <div class="text-content arrow-select">
            <p><span></span>Los Mochis</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-matamoros-tamaulipas/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Matamoros</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-mazatlan-sinaloa/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Mazatlán</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-merida-yucatan/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Mérida</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-morelia-michoacan/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Morelia</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-nuevo-laredo-tamaulipas/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Nuevo Laredo</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-puebla-puebla/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Puebla</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-queretaro/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Querétaro</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-reynosa-tamaulipas/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Reynosa</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-san-luis-potosi/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>San Luis Potosí</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-san-nicolas-monterrey-nuevo-leon/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>San Nicolas</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-toluca-estado-de-mexico/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Toluca</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-veracruz-jalapa/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Veracruz</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-villahermosa-tabasco/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Villahermosa</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/universidad-zapopan-jalisco/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Zapopan</p>
        </div>
    </div>
</a>
<a href="http://tecmilenio.mx/campus/en-linea/">
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>En línea</p>
        </div>
    </div>
</a>
</div>
</div>
<div id="admisiones-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
        </div>
    </div>
    <div class="list-part" id="admisiones2">
        <div class="text-content arrow-select">
            <p><span></span>Proceso de admisión</p>
        </div>
    </div>
    <div class="list-part" id="admisiones3">
        <div class="text-content arrow-select">
            <p><span></span>Colegiatura</p>
        </div>
    </div>
    <div class="list-part" id="admisiones4">
        <div class="text-content arrow-select">
            <p><span></span>Apoyos financieros</p>
        </div>
    </div>
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Maestrías</p>
        </div>
    </div>
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>En Línea</p>
        </div>
    </div>
    <div class="list-part">
        <div class="text-content arrow-select">
            <p><span></span>Certificados</p>
        </div>
    </div>
</div>
<div id="alumnos-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
        </div>
    </div>
    <a href="http://portal.tecmilenio.edu.mx/irj/portal">
        <div class="list-part"  >
            <div class="text-content arrow-select">
                <p class="green"><span></span>Servicios en linea</p>
            </div>
        </div>
    </a>
    <a href="http://www.tecmilenio.edu.mx/servicios/">
        <div class="list-part"  >
            <div class="text-content arrow-select green-responsive">
                <p class="green"><span></span>Portal de alumnos</p>
            </div>
        </div>
    </a>
    <a href="http://bbsistema.tecmilenio.edu.mx/">
        <div class="list-part">
            <div class="text-content arrow-select green-responsive">
                <p class="green"><span></span>Blackboard</p>
            </div>
        </div>
    </a>
    <a href="http://prueba.tecmilenio.mx.php53-10.ord1-1.websitetestlink.com//mail/">
        <div class="list-part"  >
            <div class="text-content arrow-select green-responsive">
                <p class="green"><span></span>Email</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.occ.com.mx/Bolsa_Trabajo">
        <div class="list-part">
            <div class="text-content arrow-select green-responsive">
                <p class="green"><span></span>Bolsa de trabajo</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/calendarios/maestrias/">
        <div class="list-part">
            <div class="text-content arrow-select green-responsive">
                <p class="green"><span></span>Calendario</p>
            </div>
        </div>
    </a>
</div>
<div id="egresados-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
        </div>
    </div>
    <a href="http://tecmilenio.occ.com.mx/Bolsa_Trabajo">
        <div class="list-part" style="margin-top:12%;">
            <div class="text-content arrow-select">
                <p class="green"><span></span>Bolsa de trabajo</p>
            </div>
        </div>
    </a>
    <a href="http://graduados.tecmilenio.mx/">
        <div class="list-part">
            <div class="text-content arrow-select">
                <p class="green"><span></span>Portal de graduados</p>
            </div>
        </div>
    </a>
</div>
<!---->
<!--dropdown tercer nivel-->
<div id="conoce2-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
            <p class="back-item">Universidad Tecmileno</p>
        </div>
        <h1>Universidad Tecmilenio</h1>
    </div>
    <a href="http://tecmilenio.mx/mensajes-del-rector/">
        <div class="list-part" style="margin-top:12%;">
            <div class="text-content last-arrow">
                <p>Mensaje del rector</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/nosotros/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>¿Quiénes somos?</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/videoteca/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Videoteca</p>
            </div>
        </div>
    </a>
</div>
<div id="conoce3-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
            <p class="back-item">Modelo Educativo</p>
        </div>
        <h1>Modelo Educativo</h1>
    </div>
    <a href="http://tecmilenio.mx/modelo-de-universidad/">
        <div class="list-part"style="margin-top:12%;">
            <div class="text-content last-arrow">
                <p>Nuevo Modelo Educativo</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/recorrido-virtual/">
        <div class="list-part" >
            <div class="text-content last-arrow">
                <p>¿Qué opinan los expertos?</p>
            </div>
        </div>
    </a>
</div>
<div id="conoce4-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
            <p class="back-item">Conoce más</p>
        </div>
        <h1>Conoce más</h1>
    </div>
    <a href="http://cienciasdelafelicidad.mx/">
        <div class="list-part" style="margin-top:12%;">
            <div class="text-content last-arrow">
                <p>Instituto de Ciencias de la Felicidad</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/pi/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Programas Internacionales</p>
            </div>
        </div>
    </a>
</div>
<div id="admisiones2-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
            <p class="back-item">Proceso de admisión</p>
        </div>
        <h1>Proceso de admisión</h1>
    </div>
    <a href="http://tecmilenio.mx/proceso-de-admisiones/pasos/">
        <div class="list-part" style="margin-top:12%;">
            <div class="text-content last-arrow">
                <p>Pasos</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/proceso-de-admisiones/requisitos/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Requisitos</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/proceso-de-admisiones/periodos/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Periodos de Ingreso</p>
            </div>
        </div>
    </a>
    <a href="http://inscripciones.tecmilenio-rd.com/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Feria de Inscripciones</p>
            </div>
        </div>
    </a>
</div>
<div id="admisiones3-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
            <p class="back-item">Colegiatura</p>
        </div>
        <h1>Colegiatura</h1>
    </div>
    <a href="http://tecmilenio.mx/colegiaturas/costos/">
        <div class="list-part" style="margin-top:12%;">
            <div class="text-content last-arrow">
                <p>Colegiatura</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/colegiaturas/pagos/">
        <div class="sub-nav">
            <div class="back-last back">
                <p class="back-item">Menu principal</p>
                <p class="back-item">Proceso de admisión</p>
            </div>
            <h1>Proceso de admisión</h1>
        </div>
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Pagos</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/colegiaturas/seguros/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Seguros</p>
            </div>
        </div>
    </a>
</div>
<div id="admisiones4-dropdown" class="hide-menu">
    <div class="sub-nav">
        <div class="back-last back">
            <p class="back-item">Menu principal</p>
            <p class="back-item">Apoyos financieros</p>
        </div>
        <h1>Apoyos financieros</h1>
    </div>
    <a href="http://tecmilenio.mx/apoyos-financieros/becas/tipos-de-becas/">
        <div class="list-part" style="margin-top:12%;">
            <div class="text-content last-arrow">
                <p>Becas</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/apoyos-financieros/credito-educativo/caracteristicas/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Crédito educativo</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/apoyos-financieros/plan-de-educacion-a-futuro/que-es-pef/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Plan de Educación a Futuro</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/apoyos-financieros/financiamiento-lumni/informacion-general-lumni/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Financiamiento LUMNI</p>
            </div>
        </div>
    </a>
    <a href="http://tecmilenio.mx/apoyos-financieros/convenios/">
        <div class="list-part">
            <div class="text-content last-arrow">
                <p>Convenios</p>
            </div>
        </div>
    </a>
</div>
<!---->
</div>
<!--Social header -->
<div class="social_header">
    <ul>
        <li class='icons-h chat-icon-header'><span></span><a onclick="Comm100API.open_chat_window(event, 25);" href="#"><img style="border:none;" id="comm100-button-25img"></a></li>
        <li class='icons-h header-50'><span class="phone-icon-header"></span><span class='phone-htt'><a href="tel:018002881310">01&nbsp;800&nbsp;288&nbsp;1310</a></span></li>
        <li class='icons-h header-30'>
            <ul class='sub-social-header'>
                <li class='icons-h sn-icons-header facebook-icon-header'><a href="https://www.facebook.com/UniversidadTecMilenio" target="_blank"></a></li>
                <li class='icons-h sn-icons-header twitter-icon-header'><a href="https://twitter.com/TecMilenio" target="_blank"></a></li>
                <li class='icons-h sn-icons-header youtube-icon-header'><a href="https://www.youtube.com/user/vivetecmilenio" target="_blank"></a></li>
            </ul>
        </li>
        <li class="ns-hdr icons-h">&nbsp;</li>
    </ul>
    <ul class='big-icons-h'>
        <li class='facebook-icon-header2 sn-icons-header'><a href="https://www.facebook.com/UniversidadTecMilenio" target="_blank"></a></li>
        <li class='twitter-icon-header2 sn-icons-header'><a href="https://twitter.com/TecMilenio" target="_blank"></a></li>
        <li class='youtube-icon-header2 sn-icons-header'><a href="https://www.youtube.com/user/vivetecmilenio" target="_blank"></a></li>
    </ul>
</div>
	
