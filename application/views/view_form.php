<style>

    #tm-formulario-legal {
    background: none repeat scroll 0 0 #023E7A;
    color: #FFFFFF;
    display: none;
    font-size: 14px;
    height: 480px;
    padding: 2em;
    width: 640px;
}
#colorbox #tm-formulario-legal {
    display: block;
}

#colorbox .container{
  display:  inline;

}
#tm-formulario-legal .submit input {
    background: none repeat scroll 0 0 #41C436;
    border: medium none;
    border-radius: 5px;
    color: #FFFFFF;
    padding: 10px 20px;
}
#tutor_email_field, #tutor_email_field:focus {
    border: medium none;
    padding: 0.5em 1em;
    width: 260px;
}
#tm-formulario-legal .error {
    color: #FF6666;
}
#tm-formulario-legal input {
    display: inline-block;
    margin-right: 0.3em;
}
#tm-formulario-legal input[disabled="true"] {
    background: none repeat scroll 0 0 #999999;
}

</style>

<div class="form">
    <form id="form_landing" class="tm-formulario landing" method="POST" action="<?php echo base_url()?>becas/becas/submit_landing">
                      <div class="form">
                          <form id="form_landing" class="tm-formulario landing" method="POST" action="<?php echo base_url()?>formularios/recibe/submit_landing">
                              <input type="hidden" name="action" value="submit_landing" />
                              <input type="hidden" name="tutor_email" id="tutor_email" value="" />
                              <input type="hidden" name="origen" value="<?php
                              if(isset($origen)){
                                  echo $origen;
                              }else{
                                  echo 4;
                              }
                              ?>" />
                              <input type="hidden" name="detalle_origen" value="<?php
                              if(isset($origen)){
                                  echo $origen;
                              }else{
                                  echo 11;
                              }
                              ?>" />
                              <div class="primera-tabla">
                                  <div class="text nombre">
                                      <input type="text" id="nombre" name="nombre" placeholder="Nombre(s) *" value="" data-rule-required="true" maxlength="24" />
                                  </div>
                                  <div class="text apellido">
                                      <input type="text" id="apellido_paterno" name="apellido_paterno" placeholder="Apellido paterno *" value="" data-rule-required="true" maxlength="24" />
                                  </div>
                              </div>
                              <div class="segunda-tabla">
                                  <div class="text email">
                                      <input type="email" id="email" name="email" placeholder="Correo *" value="" data-rule-required="true" data-rule-email="true" />
                                  </div>
                                  <div class="text telefono">
                                      <input type="text" id="telefono" name="telefono" placeholder="Teléfono *" value="" data-rule-required="true" data-rule-digits="true" data-rule-minlength="10" />
                                  </div>
                              </div>
                              <div class="tercera-tabla">
                                  <div class="select campus" >
                                      <select style="display:none;" id="campus" name="campus" data-rule-required="true">
                                          <option value="" selected="selected">Campus</option>
                                          <?php foreach ($campus as $result){?>
                                              <option value="<?php echo $result['id_campus']?>"><?php echo $result['campus_estado'];?></option>
                                          <?php }?>
                                          <!--  <option value="" selected="selected">Campus</option><option value='219'>Campus en Línea</option><option value='360'>Cancún</option><option value='260'>Chihuahua</option><option value='399'>Ciudad Juárez</option><option value='418'>Ciudad Obregón</option><option value='65'>Cuautitlán</option><option value='443'>Cuernavaca</option><option value='132'>Culiacán</option><option value='152'>Cumbres</option><option value='174'>Durango</option><option value='114'>Ferrería</option><option value='203'>Guadalajara</option><option value='130'>Guadalupe</option><option value='471'>Hermosillo</option><option value='544'>Laguna</option><option value='430'>Las Torres</option><option value='489'>Los Mochis</option><option value='541'>Matamoros</option><option value='569'>Mazatlán</option><option value='572'>Morelia</option><option value='664'>Mérida</option><option value='646'>Nuevo Laredo</option><option value='696'>Puebla</option><option value='611'>Querétaro</option><option value='613'>Reynosa</option><option value='659'>San Luis Potosí</option><option value='683'>San Nicolás</option><option value='774'>Toluca</option><option value='823'>Veracruz</option><option value='833'>Villahermosa</option><option value='875'>Zapopan</option> -->
                                      </select>
                                  </div>
                                  <div class="select areas">
                                      <select id="areas" name="areas" data-rule-required="true">
                                          <option value="">Cargando...</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="cuarta-tabla">
                                  <div class="select programas">
                                      <select id="programas" name="programas" data-rule-required="true">
                                          <option value="">Cargando...</option>
                                      </select>
                                  </div>
                                  <div class="submit">
                                      <input type="submit" value="Enviar datos" />
                                  </div>
                              </div>
                              <input type="hidden" id="horario" name="horario" value="" />
                              <input type="hidden" id="modalidad" name="modalidad" />
                              <input type="hidden" id="periodo" name="periodo" />
                          </form>
                          <div id="tm-formulario-legal">
                              <div class="container">
                                  <form method="post">
                                      <p><input type="checkbox" name="autenticidad" value="1" data-rule-required="true" /> Declaro que la información que proporcione a Enseñanza e Investigación Superior, A.C. (“Universidad Tecmilenio”) es auténtica y confiable, así mismo que la entrego por mi propio derecho por ser mayor de edad o a través padre/tutor por ser menor de edad. Reconozco que la “Universidad Tecmilenio” recibe la información en base a la confianza de que ésta es auténtica; no obstante estoy de acuerdo y en conocimiento que para tener la calidad de alumno actual de la “Universidad Tecmilenio” debo cumplir con todos los requisitos académicos y de documentación necesarios, por lo que autorizo a dicha Universidad para que lleve a cabo la validación y autenticación de los datos y documentos que entrego. </p>
                                      <p><input type="checkbox" name="privacidad" value="1" data-rule-required="true" /> Declaro que he leído, entendido y estoy de acuerdo con el Aviso de Privacidad. Si usted es menor de edad le recordamos que para tratar su información personal es necesario contar con el consentimiento de su padre y/o tutor. En virtud de lo anterior, si usted proporciona su información personal reconoce tener el consentimiento de su padre y/o tutor para que Universidad Tecmilenio trate ésta. No obstante lo anterior, le solicitamos nos proporcione un correo electrónico a través del cual se pueda contactar a su padre y/o tutor para poner a su disposición el presente Aviso de Privacidad.</p>
                                      <p><input type="text" style="color: #000" id="tutor_email_field" name="tutor_email_field" placeholder="Email padre o tutor (menor de edad)" data-rule-email="true" /></p>
                                      <div class="submit">
                                          <input type="submit" value="Enviar" />
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>



<script>

        jQuery(document).ready(function($){
            $('#campus').hide();
            $('#areas').hide();
            $('#programas').hide();
            
            $('#telefono').focus(function(){ $("#campus").show(); });
            
            $("#campus").change(function(){
                var id_campus=$(this).val();
               if(id_campus!=""){
                    jQuery.ajax({
                        type: "POST",
                        url: "<?php echo base_url()?>becas/becas/nivel_interes",
                        data: { id_campus: id_campus }
                    })
                     .done(function (msg) {
                         console.info(msg);
                         $areas = '<option value="">Elige tu nivel de interés</option>';
                         var myObject = eval('(' + msg + ')');
                         for (var i in myObject){
                           console.info(myObject[i]);
                            $areas += '<option value="'+myObject[i].id_nivel_interes+'">'+myObject[i].nivel_interes+'</option>';
                         }
                         $('.tm-formulario select[name="areas"]').html($areas);
        
                            $("#areas").show();
                            $("#programas").hide();
                            $programas = '<option value="">Programa</option>';
                            $("#programas").html($programas);
                            if($("#programas").is(':visible')){
                              $("#areas").show();
                            }
                    });
               } 
            });
            
            
            $("#areas").change(function(){
                var id_area=$(this).val();
                var id_campus=$("#campus").val();
               if(id_campus!="" && id_area!=""){
                    jQuery.ajax({
                        type: "POST",
                        url: "<?php echo base_url()?>becas/becas/programa_interes",
                        data: { id_campus: id_campus, id_nivel_interes: id_area }
                    })
                     .done(function (msg) {
                         console.info(msg);
                         $programas = '<option value="">Elige tu programa de interés</option>';
                         var myObject = eval('(' + msg + ')');
                         for (var i in myObject){
                           console.info(myObject[i]);
                             /*w=$(window).width();
                             len=20;
                             if(w>485)
                             if(myObject[i].programa_interes.length)
                                var name_p=myObject[i].programa_interes.substr(0,20)+"...";
                             else
                                 var name_p=myObject[i].programa_interes;*/
                            $programas += '<option class="option_overflow" value="'+myObject[i].id_programa_interes+'">'+myObject[i].programa_interes+'</option>';
                            modalidad[myObject[i].id_programa_interes]=myObject[i].id_modalidad;
                            periodo[myObject[i].id_programa_interes]=myObject[i].id_periodo;
                         }
                                
        
                            $("#programas").html($programas);
                            $("#programas").show();
                    });
               } 
            });
            
            $("#programas").change(function(){
                var id=$(this).val();
                $("#modalidad").val(modalidad[id]);
                $("#periodo").val(periodo[id]);
                
            });

            $(".tm-formulario").validate({
                submitHandler: function(form) {
                  $.colorbox({inline:true, href: '#tm-formulario-legal'});
            			return false;
            		}
              });
            
            $('#tm-formulario-legal form').validate({
            submitHandler: function(form) {
              $('#tm-formulario-legal .submit input').hide();
              $('#tutor_email').val($('#tutor_email_field').val());
        			jQuery('.tm-formulario').ajaxSubmit({url: $(".tm-formulario").attr('action') , success: function(data){
                       window.location.href ="<?php if(isset($gracias)){echo $gracias;}?>";
                      console.info(data);  
          			
        			}});
        			
        			return false;
        		}
          });
            
            
            
        });
        

</script>