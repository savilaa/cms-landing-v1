<!doctype html>
<html lang="es-ES">
<meta charset="UTF-8">
<meta name="viewport" content="width=480px"/>
<head>
    <title>Becas Talento - Universidad Tecmilenio</title>
    <!-- Google Analytics Content Experiment code -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-5964406-1', 'tecmilenio.mx');
        ga('send', 'pageview');

    </script>
    <?php if(isset($tipo)){ ?>
        <!-- Google Analytics Content Experiment code -->
        <script>function utmx_section(){}function utmx(){}(function(){var
                k='82682437-3',d=document,l=d.location,c=d.cookie;
                if(l.search.indexOf('utm_expid='+k)>0)return;
                function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
                    indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
                    length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
                    '<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
                        '://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
                        '&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
                        valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
                        '" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
        </script><script>utmx('url','A/B');</script>
        <!-- End of Google Analytics Content Experiment code -->
        <script type="text/javascript">
            // <![CDATA[
            //setTimeout( "cambia_body()", 5000 );

            document.addEventListener("DOMContentLoaded", function(){
                var IE10 = navigator.userAgent.toString().toLowerCase().indexOf("trident/6")>-1;
                var IE11 = !!navigator.userAgent.match(/Trident.*rv[ :]*11\./);
                if (IE10){
                    var x = document.getElementsByTagName("body");
                    console.info(x[0].className);
                    x[0].className = x[0].className+" ie10";
                    //alert("ie10");
                }
                if (IE11) {
                    var x = document.getElementsByTagName("body");
                    console.info(x[0].className);
                    x[0].className = x[0].className+" ie11";
                    //alert("ie11");
                }
            }, true);
            // ]]>
        </script>
    <?php
    }
    ?>

    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
    <link rel="shortcut icon" href="<?=base_url()?>assets/img/favicon.ico" />
    <link rel='stylesheet' id='html5blank-css'  href='http://tecmilenio.mx/wp-content/themes/tecmilenio/style.css' media='all' />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/menu-responsivo2.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/header-style.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/universal.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/base.css">
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery-1.10.2.min.js?ver=1.10.2'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/tm-formularios/jquery.validate.min.js?ver=3.6'></script>
    <!--<script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/tm-formularios/jquery.placeholder.js?ver=3.6'></script>-->
    <link rel='stylesheet' id='colorbox-css'  href='http://tecmilenio.mx/wp-content/themes/tecmilenio/css/colorbox/colorbox.css?ver=1.0' media='all' />
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/tm-formularios/messages_es.js?ver=3.6'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-includes/js/jquery/ui/jquery.ui.core.min.js?ver=1.10.3'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-includes/js/jquery/ui/jquery.ui.datepicker.min.js?ver=1.10.3'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/wp-views/embedded/res/js/i18n/jquery.ui.datepicker-es.js?ver=1.2.2'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/wp-views/embedded/res/js/wpv-date-front-end-control.js?ver=1.2.2'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-includes/js/swfobject.js?ver=2.2-20120417'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/greensock/TweenMax.min.js?ver=1.0.0'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/conditionizr.min.js?ver=3.0.0'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/modernizr.min.js?ver=2.6.2'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery.columnizer.js?ver=2.6.2'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery.nivo.slider.js?ver=1.0.0'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery.colorbox-min.js?ver=1.0.0'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/scripts.js?ver=1.0.0'></script>
    <script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery.mCustomScrollbar.min.js?ver=1.0.0'></script>

    <script type="text/javascript">
        var front_ajaxurl = 'http://tecmilenio.mx/wp-admin/admin-ajax.php';
        var wpv_calendar_image = 'http://tecmilenio.mx/wp-content/plugins/wp-views/embedded/res/img/calendar.gif';
        var wpv_calendar_text = 'Seleccione la fecha';
    </script>
    <script>
        $(document).ready(function(){
            $( "#menu-rp" ).click(function() {
                if($( "#menu-dropdown").css('display')=='none'){
                    $( "#menu-dropdown" ).fadeIn("slow");
                    $( ".wrapper" ).hide();
                }
                else {
                    $( ".hide-menu" ).fadeOut("slow");
                    $( ".wrapper" ).show();
                }
            });
            var regresa=0;
            var clickedId="";
            var clickedId2="";
            $(".list-part").click(function() {
                if(regresa==0){
                    clickedId= $(this).attr("id");
                    $("#"+clickedId+"-dropdown").fadeIn('slow');
                }else{
                    clickedId2= $(this).attr("id");
                    $("#"+clickedId2+"-dropdown").fadeIn('slow');
                }
                regresa++;
            });
            $(".back").click(function() {
                if(regresa>1){
                    $("#"+clickedId2+"-dropdown").hide("slow");
                    regresa--;
                }else{
                    $("#"+clickedId+"-dropdown").hide("slow");
                }

            });
        });
    </script>
    <style>
        button, input, select, textarea {
            font-size: 100%;
            margin: 0;
            vertical-align: baseline;
        }
        .phone-htt{ font-size: 26px;}
        @media screen and(max-width: 480px){
            .phone-htt{
                font-size: 22px !important;
                margin-top: 10px;
            }}
    </style>

</head>
<?php if(isset($header)) echo $header;?>
<div class="wrapper">
    <div class="row">
        <div class="content-section col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="images-header col-lg-7 col-md-7 col-sm-5 col-xs-5">
                <img class="responsive-img" src="<?=base_url()?>assets/img/becas/Foto_BecasTalento.png" alt=""/>
            </div>
                <div class="texto-desc col-lg-5 col-md-5 col-sm-7 col-xs-7">
                    <h1 class='h1-text-desc'>Apreciamos tu talento</h1>
                    <p class="p-text-desc"> Conoce nuestras becas y Apoyos Financieros</p>
                    <div class="play1 col-lg-12 col-md-12 col-sm-6 col-xs-9 no-padd">
                        <a class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padd" href="#video-landing"><img class="responsive-img" src="http://localhost:8080/ci/assets/img/play.png"></a>
                        <p class="col-lg-8 col-md-8 col-sm-8 col-xs-8 no-padd">Por qué estudiar<br>
                            <b>en Tecmilenio</b><br>
                            <a href="#video-landing">Ver video</a>
                        </p>
                    </div>
                </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-11 col-xs-12">
            <img src="<?=base_url()?>assets/img/solicita.png" alt= "..." class="img-responsive">
            <?php if(isset($content)){echo $content;} ?>
        </div>
    </div>
    <div class="row">
        <div class="back-img col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <figure class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                    <img src="<?=base_url()?>assets/img/licenciatura.png" alt="">
                    <h2 class="title-barra-verde">Carreras</h2>
                </figure>
                <?php
                    $programas_el = array("Enfermería","Nutrición", "Psicología","Gastronomía", "Diseño Gráfico y Animación","Derecho", "Administración de Empresas","Administración Financiera", "Administración Hotelera y Turística", "Comercio Intencional");
                    $programas_ed = array("Desarrollo de Software","Industrial", "Mecatrónica", "Sistemas de Computación Administrativa");
                    $langp = count($programas_el);
                    $langd = count($programas_ed);
                    $counterback= $langp*25;
                echo "<style> @media screen and (min-width:769px){";
                    if ($langp>=8) { echo  ".back-img figure{padding-top:10%;} ";}
                    if ($langp>=10) { echo  ".back-img figure{padding-top:13%;} ";}

                echo ".lista-barra-verde{ height: ".$counterback."px;}
                }</style> ";
                echo '<div class="col-lg-8 col-md-8 col-sm-7 col-xs-9 separador-bv"> ';
                echo '<ul class="lista-barra-verde">';
                foreach ($programas_el as $lang){ 
                echo '<li><a href="target=_blank;">'.$lang.'</a></li>'; }
                echo'</ul>';
                ?>
                <!-- <div class="col-lg-8 col-md-8 col-sm-7 col-xs-9 separador-bv">
                    <ul class="lista-barra-verde">
                        <li><a href="" target="_blank;">Enfermería</a></li>
                        <li><a href="" target="_blank";>Nutrición</a></li>
                        <li><a href="" target="_blank;">Psicología</a></li>
                        <li><a href="" target="_blank;">Gastronomía</a></li>
                        <li><a href="" target="_blank;">Diseño Gráfico y Animación</a></li>
                        <li><a href="" target="_blank;">Derecho</a></li>
                        <li><a href="" target="_blank;">Administración de Empresas </a></li>
                        <li><a href="" target="_blank;">Administración Financiera </a></li>
                        <li><a href="" target="_blank;">Administra Hotelera y Turística </a></li>
                        <li><a href="" target="_blank;">Comercio Intencional </a></li>
                    </div></ul> -->
                </div>
            </div>
            <div class=" col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <figure class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                    <img src="<?=base_url()?>assets/img/ingenieria.png" alt="">
                    <h2 class="title-barra-verde">Ingenierías</h2>
                </figure>
                <div class=" col-xs-9 col-lg-8 col-md-7 col-sm-5 separador-bv">
                    <ul class="lista-barra-verde">
                        <li><a href="" target="_blank;">Desarrollo de Software</a></li>
                        <li><a href="" target="_blank;">Industrial</a></li>
                        <li><a href="" target="_blank;">Mecatrónica</a></li>
                        <li><a href="" target="_blank;">Sistemas de Computación Administrativa </a></li>
                    </ul>
                </div>
            </div>
        </div>

    <div class="back-cont col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <section class="beneficios">
            <div class="row">
                <div class="sub-text col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <h2>Beneficios</h2>
                </div>
                <div class="b-details col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
                    <figure class="col-xs-4 col-lg-11 col-md-11 col-sm-12">
                        <img src="<?=base_url()?>assets/img/becas/beneficio-01.png" alt="">
                    </figure>
                    <strong>Beca al Mérito Académico</strong>
                    <p class="col-xs-8 col-lg-11 col-md-11 col-sm-12 text-center">
                        <span>Para alumnos de preparatoria o profesional con un promedio igual o superior a 90 en el periodo académico o nivel de estudios anterior.<br>
                        <small>*No esta disponible para estudios de Posgrado.</small>
                        </span>
                    </p>
                </div>
                <div class="b-details col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
                    <figure class="col-xs-4 col-lg-11 col-md-11 col-sm-12">
                        <img src="<?=base_url()?>assets/img/becas/beneficio-02.png" alt="">
                    </figure>
                    <strong>Beca Académica</strong>
                    <p class="col-xs-8 col-lg-11 col-md-11 col-sm-12 text-center">
                        <span>Es aquella a la que pueden aplicar los alumnos de preparatoria, profesional o posgrado con un promedio igual o superior a 80 en el periodo académico o nivel de estudios anterior.</span>
                    </p>
                </div>
                <div class="b-details col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
                    <figure class="col-xs-4 col-lg-11 col-md-11 col-sm-12">
                        <img src="<?=base_url()?>assets/img/becas/beneficio-03.png" alt="">
                    </figure>
                    <strong>Beca Familiar</strong>
                    <p class="col-xs-8 col-lg-11 col-md-11 col-sm-12 text-center">
                        <span>Es la aportación que otorga la universidad Tecmilenio a aquellos alumnos de preparatoria, profesional o posgrado, que iniciarán estudios en un campus en el que al menos un hermano(a) de éste ya se encuentre estudiando sin tener beneficio de alguna beca.<span>
                </div>
                <div class="b-details col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
                    <figure class="col-xs-4 col-lg-11 col-md-11 col-sm-12">
                        <img src="<?=base_url()?>assets/img/becas/beneficio-04.png" alt="">
                    </figure>
                    <strong>Beca del Esfuerzo</strong>
                    <p class="col-xs-8 col-lg-11 col-md-11 col-sm-12 text-center">
                        <span>Para alumnos de preparatoria, profesional o posgrado, que demuestren estar laborando actualmente y que estudien en el turno nocturno.</span>
                    </p>
                </div>
                <section class="slider-oculto col-xs-12">
                    <div class="device">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <!-- -->
                                <div class="swiper-slide">
                                    <div class="contenido">
                                        <img src="<?=base_url()?>assets/img/becas/beneficio-01.png" alt="">
                                        <h2 class="tmilenio">Beca al Mérito Académico</h2>
                                        <p>Para alumnos de preparatoria o profesional con un promedio igual o superior a 90 en el periodo académico o nivel de estudios anterior. <br>
                                            <small>*No esta disponible para estudios de Posgrado.</small>
                                        </p>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="contenido">
                                        <img src="<?=base_url()?>assets/img/becas/beneficio-02.png" alt="">
                                        <h2 class="tmilenio">Beca Académica</h2>
                                        <p>Es aquella a la que pueden aplicar los alumnos de preparatoria, profesional o posgrado con un promedio igual o superior a 80 en el periodo académico o nivel de estudios anterior.</p>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="contenido">
                                        <img src="<?=base_url()?>assets/img/becas/beneficio-03.png" alt="">
                                        <h2 class="tmilenio">Beca Familiar</h2>
                                        <p>Es la aportación que otorga la universidad Tecmilenio a aquellos alumnos de preparatoria, profesional o posgrado, que iniciarán estudios en un campus en el que al menos un hermano(a) de éste ya se encuentre estudiando sin tener beneficio de alguna beca.</p>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="contenido">
                                        <img src="<?=base_url()?>assets/img/becas/beneficio-04.png" alt="">
                                        <h2 class="tmilenio">Beca del Esfuerzo</h2>
                                        <p>Para alumnos de preparatoria, profesional o posgrado, que demuestren estar laborando actualmente y que estudien en el turno nocturno.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pag-container">
                            <a class="arrow-left arrow-bg" href="#"></a>
                            <div class="pagination"></div>
                            <a class="arrow-right arrow-bg" href="#"></a>
                        </div>
                    </div>
                </section>
            </div>
        </section>
        <div class="row">
            <div id="video-landing" class="video-landing col-xs-12 col-lg-12 col-md-12 col-sm-12">
                <iframe max-width="1200" height="515" width="100%" src="//www.youtube.com/embed/839vN_KXkzw" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="col-lg-1">
                        <input id="terminos_1" type="checkbox">
                    </div>
                    <div class="col-lg-11 terms">
                        <p>
                            Declaro que la información que proporcione a Enseñanza e Investigación Superior, A.C. (“Universidad Tecmilenio”) es auténtica y confiable, así mismo que la entrego por mi propio derecho por ser mayor de edad o a través padre/tutor por ser menor de edad. Reconozco que la “Universidad Tecmilenio” recibe la información en base a la confianza de que ésta es auténtica; no obstante estoy de acuerdo y en conocimiento que para tener la calidad de alumno actual de la “Universidad Tecmilenio” debo cumplir con todos los requisitos académicos y de documentación necesarios, por lo que autorizo a dicha Universidad para que lleve a cabo la validación y autenticación de los datos y documentos que entrego.
                        </p>
                    </div>
                    <div class="col-lg-1">
                        <input id="terminos_2" type="checkbox">
                    </div>
                    <div class="col-lg-11 terms">
                        <p>
                            Declaro que he leído, entendido y estoy de acuerdo con el Aviso de Privacidad. Si usted es menor de edad le recordamos que para tratar su información personal es necesario contar con el consentimiento de su padre y/o tutor. En virtud de lo anterior, si usted proporciona su información personal reconoce tener el consentimiento de su padre y/o tutor para que Universidad Tecmilenio trate ésta. No obstante lo anterior, le solicitamos nos proporcione un correo electrónico a través del cual se pueda contactar a su padre y/o tutor para poner a su disposición el presente Aviso de Privacidad.
                        </p>
                    </div>
                </div>
                <input type="email" class="form-control email-modal" id="email_mentor" placeholder="E-mail del padre o tutor (Sólo si eres menor de edad)">
                <button id="enviar2" type="submit" class="btn btn-default align-right enviar" data-toggle="modal" data-target="#myModal">Enviar</button>

            </div>
        </div>
    </div>
</div>

<?php echo $footer 
/*
variables disponibles
<pre>
print_r($datos)

 : titulo
 : imagen
</pre>


<p><?=helper_titulos_ajustados($datos->titulo);?></p>
*/
?>