CREATE DATABASE  IF NOT EXISTS `page_landing` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `page_landing`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: page_landing
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `areas_academicas`
--

DROP TABLE IF EXISTS `areas_academicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas_academicas` (
  `id_area_academica` int(11) NOT NULL AUTO_INCREMENT,
  `area_academica` varchar(50) NOT NULL,
  `imagen_area_academica` varchar(45) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_nivel_interes` int(11) NOT NULL,
  PRIMARY KEY (`id_area_academica`,`id_nivel_interes`),
  KEY `fk_areas_academicas_nivel_interes1_idx` (`id_nivel_interes`),
  CONSTRAINT `fk_areas_academicas_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas_academicas`
--

LOCK TABLES `areas_academicas` WRITE;
/*!40000 ALTER TABLE `areas_academicas` DISABLE KEYS */;
/*!40000 ALTER TABLE `areas_academicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beneficios`
--

DROP TABLE IF EXISTS `beneficios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beneficios` (
  `id_beneficios` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(45) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `content` longtext NOT NULL,
  `valor` varchar(2) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_nivel_interes` int(11) NOT NULL,
  PRIMARY KEY (`id_beneficios`,`id_nivel_interes`),
  KEY `fk_beneficios_nivel_interes1_idx` (`id_nivel_interes`),
  CONSTRAINT `fk_beneficios_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beneficios`
--

LOCK TABLES `beneficios` WRITE;
/*!40000 ALTER TABLE `beneficios` DISABLE KEYS */;
/*!40000 ALTER TABLE `beneficios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id_campus` int(11) NOT NULL AUTO_INCREMENT,
  `clave_campus` varchar(45) NOT NULL,
  `campus` varchar(40) NOT NULL,
  `campus_estado` varchar(60) NOT NULL,
  `Ing` varchar(30) NOT NULL,
  `lat` varchar(30) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_campus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_origen`
--

DROP TABLE IF EXISTS `detalle_origen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_origen` (
  `id_detalle_origen` int(11) NOT NULL AUTO_INCREMENT,
  `clave_detalle_origen` int(2) NOT NULL,
  `detalle_origen` varchar(50) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_detalle_origen`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_origen`
--

LOCK TABLES `detalle_origen` WRITE;
/*!40000 ALTER TABLE `detalle_origen` DISABLE KEYS */;
INSERT INTO `detalle_origen` VALUES (1,34,'Becas Talento','activo','2014-09-08 11:38:00');
/*!40000 ALTER TABLE `detalle_origen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formulario`
--

DROP TABLE IF EXISTS `formulario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formulario` (
  `id_formulario` int(11) NOT NULL AUTO_INCREMENT,
  `formulario_nombre` varchar(45) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_formulario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formulario`
--

LOCK TABLES `formulario` WRITE;
/*!40000 ALTER TABLE `formulario` DISABLE KEYS */;
/*!40000 ALTER TABLE `formulario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `landing`
--

DROP TABLE IF EXISTS `landing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `landing` (
  `id_landing` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(80) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `frase` varchar(80) NOT NULL,
  `img_principal` varchar(45) NOT NULL,
  `beneficios_titulo` varchar(60) NOT NULL,
  `video` varchar(45) NOT NULL,
  `codigo_conversion` varchar(45) NOT NULL,
  `label` varchar(45) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_formulario` int(11) NOT NULL,
  `id_origen` int(11) NOT NULL,
  `id_detalle_origen` int(11) NOT NULL,
  `id_nivel_interes` int(11) NOT NULL,
  PRIMARY KEY (`id_landing`,`id_formulario`,`id_origen`,`id_detalle_origen`,`id_nivel_interes`),
  KEY `fk_landing_origen1_idx` (`id_origen`),
  KEY `fk_landing_fomulario1_idx` (`id_formulario`),
  KEY `fk_landing_detalle_origen1_idx` (`id_detalle_origen`),
  KEY `fk_landing_nivel_interes1_idx` (`id_nivel_interes`),
  CONSTRAINT `fk_landing_detalle_origen1` FOREIGN KEY (`id_detalle_origen`) REFERENCES `detalle_origen` (`id_detalle_origen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_landing_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_landing_origen1` FOREIGN KEY (`id_origen`) REFERENCES `origen` (`id_origen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_landing_fomulario1` FOREIGN KEY (`id_formulario`) REFERENCES `formulario` (`id_formulario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `landing`
--

LOCK TABLES `landing` WRITE;
/*!40000 ALTER TABLE `landing` DISABLE KEYS */;
/*!40000 ALTER TABLE `landing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modalidad`
--

DROP TABLE IF EXISTS `modalidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modalidad` (
  `id_modalidad` int(11) NOT NULL AUTO_INCREMENT,
  `clave_modalidad` varchar(5) NOT NULL,
  `modalidad` varchar(50) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_periodo` int(11) NOT NULL,
  PRIMARY KEY (`id_modalidad`,`id_periodo`),
  KEY `fk_modalidad_periodo1_idx` (`id_periodo`),
  CONSTRAINT `fk_modalidad_periodo1` FOREIGN KEY (`id_periodo`) REFERENCES `periodo` (`id_periodo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modalidad`
--

LOCK TABLES `modalidad` WRITE;
/*!40000 ALTER TABLE `modalidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `modalidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nivel_interes`
--

DROP TABLE IF EXISTS `nivel_interes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nivel_interes` (
  `id_nivel_interes` int(11) NOT NULL AUTO_INCREMENT,
  `clave_nivel_interes` varchar(45) NOT NULL,
  `nivel_interes` varchar(100) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_nivel_interes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nivel_interes`
--

LOCK TABLES `nivel_interes` WRITE;
/*!40000 ALTER TABLE `nivel_interes` DISABLE KEYS */;
/*!40000 ALTER TABLE `nivel_interes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ofrece`
--

DROP TABLE IF EXISTS `ofrece`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ofrece` (
  `id_ofrece` int(11) NOT NULL,
  `tipo` enum('unico','otro','ambos') NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_campus` int(11) NOT NULL,
  `id_programas_interes` int(11) NOT NULL,
  `id_nivel_interes` int(11) NOT NULL,
  `id_modalidad` int(11) NOT NULL,
  PRIMARY KEY (`id_ofrece`,`id_campus`,`id_programas_interes`,`id_nivel_interes`,`id_modalidad`),
  KEY `fk_ofrece_modalidad1_idx` (`id_modalidad`),
  KEY `fk_ofrece_campus1_idx` (`id_campus`),
  KEY `fk_ofrece_programa_interes1_idx` (`id_programas_interes`),
  KEY `fk_ofrece_nivel_interes1_idx` (`id_nivel_interes`),
  CONSTRAINT `fk_ofrece_modalidad1` FOREIGN KEY (`id_modalidad`) REFERENCES `modalidad` (`id_modalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ofrece_campus1` FOREIGN KEY (`id_campus`) REFERENCES `campus` (`id_campus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ofrece_programa_interes1` FOREIGN KEY (`id_programas_interes`) REFERENCES `programa_interes` (`id_programas_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ofrece_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ofrece`
--

LOCK TABLES `ofrece` WRITE;
/*!40000 ALTER TABLE `ofrece` DISABLE KEYS */;
/*!40000 ALTER TABLE `ofrece` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `origen`
--

DROP TABLE IF EXISTS `origen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `origen` (
  `id_origen` int(11) NOT NULL AUTO_INCREMENT,
  `clave_origen` int(2) NOT NULL,
  `origen` varchar(50) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_origen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `origen`
--

LOCK TABLES `origen` WRITE;
/*!40000 ALTER TABLE `origen` DISABLE KEYS */;
/*!40000 ALTER TABLE `origen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodo`
--

DROP TABLE IF EXISTS `periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodo` (
  `id_periodo` int(11) NOT NULL AUTO_INCREMENT,
  `clave_periodo` varchar(5) NOT NULL,
  `periodo` varchar(45) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_periodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodo`
--

LOCK TABLES `periodo` WRITE;
/*!40000 ALTER TABLE `periodo` DISABLE KEYS */;
/*!40000 ALTER TABLE `periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programa_interes`
--

DROP TABLE IF EXISTS `programa_interes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programa_interes` (
  `id_programas_interes` int(11) NOT NULL,
  `clave_programa_interes` varchar(5) NOT NULL,
  `programa_interes` varchar(50) NOT NULL,
  `url_programa_interes` varchar(60) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_nivel_interes` int(11) NOT NULL,
  `id_area_academica` int(11) NOT NULL,
  PRIMARY KEY (`id_programas_interes`,`id_nivel_interes`,`id_area_academica`),
  KEY `fk_programa_interes_nivel_interes1_idx` (`id_nivel_interes`),
  KEY `fk_programa_interes_areas_academicas1_idx` (`id_area_academica`),
  CONSTRAINT `fk_programa_interes_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_programa_interes_areas_academicas1` FOREIGN KEY (`id_area_academica`) REFERENCES `areas_academicas` (`id_area_academica`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programa_interes`
--

LOCK TABLES `programa_interes` WRITE;
/*!40000 ALTER TABLE `programa_interes` DISABLE KEYS */;
/*!40000 ALTER TABLE `programa_interes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro`
--

DROP TABLE IF EXISTS `registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro` (
  `id_registro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `email` varchar(35) NOT NULL,
  `tutor_email` varchar(35) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_campus` int(11) NOT NULL,
  `id_nivel_interes` int(11) NOT NULL,
  `id_programas_interes` int(11) NOT NULL,
  `id_origen` int(11) NOT NULL,
  `id_detalle_origen` int(11) NOT NULL,
  `id_periodo` int(11) NOT NULL,
  `id_modalidad` int(11) NOT NULL,
  PRIMARY KEY (`id_registro`,`id_campus`,`id_nivel_interes`,`id_programas_interes`,`id_origen`,`id_detalle_origen`,`id_periodo`,`id_modalidad`),
  KEY `fk_registro_campus1_idx` (`id_campus`),
  KEY `fk_registro_detalle_origen1_idx` (`id_detalle_origen`),
  KEY `fk_registro_origen1_idx` (`id_origen`),
  KEY `fk_registro_programa_interes1_idx` (`id_programas_interes`),
  KEY `fk_registro_periodo1_idx` (`id_periodo`),
  KEY `fk_registro_modalidad1_idx` (`id_modalidad`),
  KEY `fk_registro_nivel_interes1_idx` (`id_nivel_interes`),
  CONSTRAINT `fk_registro_campus1` FOREIGN KEY (`id_campus`) REFERENCES `campus` (`id_campus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_detalle_origen1` FOREIGN KEY (`id_detalle_origen`) REFERENCES `detalle_origen` (`id_detalle_origen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_origen1` FOREIGN KEY (`id_origen`) REFERENCES `origen` (`id_origen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_programa_interes1` FOREIGN KEY (`id_programas_interes`) REFERENCES `programa_interes` (`id_programas_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_periodo1` FOREIGN KEY (`id_periodo`) REFERENCES `periodo` (`id_periodo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_modalidad1` FOREIGN KEY (`id_modalidad`) REFERENCES `modalidad` (`id_modalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro`
--

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;
/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `table1`
--

DROP TABLE IF EXISTS `table1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table1` (
  `idtable1` int(11) NOT NULL,
  PRIMARY KEY (`idtable1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table1`
--

LOCK TABLES `table1` WRITE;
/*!40000 ALTER TABLE `table1` DISABLE KEYS */;
/*!40000 ALTER TABLE `table1` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-08 19:04:48
