<!DOCTYPE html>
<html style="height: 100%;" lang="es-ES" prefix="og: http://ogp.me/ns#" class="no-js">
	<head>
		<meta charset="UTF-8">
		<title>Gracias - Universidad Tecmilenio</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-5964406-1', 'tecmilenio.mx');
            ga('send', 'pageview');

        </script>
        <meta name="description" content="">
        <meta content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        
        <link rel="shortcut icon" href="http://tecmilenio.mx/wp-content/themes/tecmilenio/favicon.ico" />
        <link rel="stylesheet" href="<?=base_url()?>assets/universal/css/bootstrap.min.css">
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        
<link rel='stylesheet' id='views-pagination-style-css'  href='http://tecmilenio.mx/wp-content/plugins/wp-views/embedded/res/css/wpv-pagination.css?ver=1.2.2' media='all' />
<link rel='stylesheet' id='nexabold-css'  href='http://tecmilenio.mx/wp-content/themes/tecmilenio/fonts/nexafree_bold_spanish/stylesheet.css?ver=1.0' media='all' />
<link rel='stylesheet' id='nexalight-css'  href='http://tecmilenio.mx/wp-content/themes/tecmilenio/fonts/nexafree_light_spanish/stylesheet.css?ver=1.0' media='all' />
<link rel='stylesheet' id='normalize-css'  href='http://tecmilenio.mx/wp-content/themes/tecmilenio/normalize.css?ver=1.0' media='all' />
<link rel='stylesheet' id='nivo-slider-css'  href='http://tecmilenio.mx/wp-content/themes/tecmilenio/css/nivo-slider.css?ver=1.0' media='all' />
<link rel='stylesheet' id='colorbox-css'  href='http://tecmilenio.mx/wp-content/themes/tecmilenio/css/colorbox/colorbox.css?ver=1.0' media='all' />
<link rel='stylesheet' id='landing-css'  href='http://tecmilenio.mx/wp-content/themes/tecmilenio/css/landings.css?ver=1.0' media='all' />
<link rel='stylesheet' id='custom-scrollbar-css-css'  href='http://tecmilenio.mx/wp-content/themes/tecmilenio/css/jquery.mCustomScrollbar.css?ver=1.0' media='all' />
<link rel='stylesheet' id='wpgmaps-style-css'  href='http://tecmilenio.mx/wp-content/plugins/wp-google-maps/css/wpgmza_style.css?ver=3.6' media='all' />
<link rel='stylesheet' id='wpv_render_css-css'  href='http://tecmilenio.mx/wp-content/plugins/wp-views/res/css/wpv-views-sorting.css?ver=3.6' media='all' />
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery-1.10.2.min.js?ver=1.10.2'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/tm-formularios/jquery.validate.min.js?ver=3.6'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/tm-formularios/jquery.placeholder.js?ver=3.6'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/tm-formularios/messages_es.js?ver=3.6'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-includes/js/jquery/jquery.form.min.js?ver=2.73'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var myAjax = {"ajaxurl":"http:\/\/tecmilenio.mx\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/tm-formularios/script.js?ver=1.2'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-includes/js/jquery/ui/jquery.ui.core.min.js?ver=1.10.3'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-includes/js/jquery/ui/jquery.ui.datepicker.min.js?ver=1.10.3'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/wp-views/embedded/res/js/i18n/jquery.ui.datepicker-es.js?ver=1.2.2'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/wp-views/embedded/res/js/wpv-date-front-end-control.js?ver=1.2.2'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-includes/js/swfobject.js?ver=2.2-20120417'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/greensock/TweenMax.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/conditionizr.min.js?ver=3.0.0'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/modernizr.min.js?ver=2.6.2'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery.columnizer.js?ver=2.6.2'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery.nivo.slider.js?ver=1.0.0'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery.colorbox-min.js?ver=1.0.0'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/scripts.js?ver=1.0.0'></script>
<script type='text/javascript' src='http://tecmilenio.mx/wp-content/themes/tecmilenio/js/jquery.mCustomScrollbar.min.js?ver=1.0.0'></script>
        <script type="text/javascript">
            var front_ajaxurl = 'http://tecmilenio.mx/wp-admin/admin-ajax.php';
            var wpv_calendar_image = 'http://tecmilenio.mx/wp-content/plugins/wp-views/embedded/res/img/calendar.gif';
            var wpv_calendar_text = 'Seleccione la fecha';
        </script>



 

             
    </head>
    <body class="page page-id-2042 page-child parent-pageid-2036 page-template page-template-landing-gracias-php gracias">
    <div id="wrapper">
        <div class="header-container">
            <header class="wrapper clearfix">
                <h1 class="title"><img src="http://tecmilenio.mx/wp-content/themes/tecmilenio/img/landings/universidad-tecmilenio.png" width="259" height="59" /></h1>
            </header>
        </div>
        <div class="main-container">
            <div class="main wrapper clearfix row">
                <article style="text-align:center;" class='col-lg-12 col-sm-12 col-xs-12'>
                    <img class='img-responsive' src="http://tecmilenio.mx/wp-content/themes/tecmilenio/img/landings/gracias.png"/>
                </article>
            </div> <!-- #main -->
        </div> <!-- #main-container -->
        <div class="footer-container">
            <footer class="wrapper">
                <p>
                    <strong>UNIVERSIDAD TECMILENIO</strong> D.R.© Enseñanza e Investigación Superior, A.C. Monterrey, N.L. México. 2012. <a href="/aviso-de-privacidad/">Aviso</a> y <a href="/politicas-de-privacidad/">Política de Privacidad</a>.
                    <img usemap="#socialmap" class="siguenos" src="http://tecmilenio.mx/wp-content/themes/tecmilenio/img/home/footer-siguenos.png" />
                    <map id="socialmap" name="socialmap">
                        <area shape="rect" coords="58,0,88,22" href="https://www.facebook.com/UniversidadTecMilenioProfesional" alt="Facebook" title="Facebook"    />
                        <area shape="rect" coords="92,0,122,22" href="https://twitter.com/TecMilenio " alt="Twitter" title="Twitter" />
                        <area shape="rect" coords="122,0,146,22" href="http://www.youtube.com/vivetecmilenio" alt="Youtube" title="Youtube"    />
                    </map>
                </p>
            </footer>
        </div>
    </div>
		<script type='text/javascript' src='http://tecmilenio.mx/wp-content/plugins/wp-views/embedded/res/js/wpv-pagination-embedded.js?ver=1.2.2'></script>
        <script>
        jQuery(document).ready(function($){
          //$('.texto').mCustomScrollbar();
          $('.horario label').click(function(){
            $('#horario').show();
          });
          
          $('#opciones li').click(function(){
            $('#opciones li').removeClass('active');
            $(this).addClass('active');
            var index = $('#opciones li').index(this);
            $('.texto').hide();
            $('.texto').eq(index).show();
          });
          
        });
        </script>

    </body>
</html>