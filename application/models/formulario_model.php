<?php
class Formulario_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    
    function get_campus()
    {
        $query = $this->db->get('campus');
        
        return $query;
         
    }
    function get_origen()
    {
        $query = $this->db->get('clave_origen');

        return $query;

    }
    function get_detalle_origen()
    {
        $query = $this->db->get('clave_detalle_origen');

        return $query;
    }

    
    function get_nivel_interes($id_campus)
    {
        $sql = "SELECT nivel_interes, id_nivel_interes from concentrado_view 
                WHERE id_campus=? GROUP BY nivel_interes;";
        $query=$this->db->query($sql, array($id_campus));
        
        return $query;
         
    }
    
    function get_programa_interes($id_campus,$id_programa_interes)
    {
        $sql = "SELECT id_programa_interes, programa_interes, id_modalidad, id_periodo from concentrado_view 
WHERE id_campus=? AND id_nivel_interes=? GROUP BY programa_interes;";
        $query=$this->db->query($sql, array($id_campus,$id_programa_interes));

        return $query;

    }

    function insert_entry()
    {
        $this->title   = $_POST['title']; // please read the below note
        $this->content = $_POST['content'];
        $this->date    = time();

        $this->db->insert('entries', $this);
    }

    function update_entry()
    {
        $this->title   = $_POST['title'];
        $this->content = $_POST['content'];
        $this->date    = time();

        $this->db->update('entries', $this, array('id' => $_POST['id']));
    }
    
     function insert_registros($id_campus,$id_nivel_interes,$id_programa_interes,$id_modalidad,$id_periodo,$nombre,$apellido_paterno,$email,$telefono,$origen,$tutor_email){
       date_default_timezone_set('America/Mexico_City');
              
              $now   = new DateTime('NOW');
              $fecha_de_registro =  $now->format('Y/m/d H:i'); 
        
        $data = array(
				'id_campus'            => $id_campus,
				'id_nivel_interes'     => $id_nivel_interes,
				'id_programa_interes'  => $id_programa_interes,
				'id_modalidad'         => $id_modalidad ,
				'id_periodo'           => $id_periodo,
				'nombre'               => $nombre,
				'apellido_paterno'     => $apellido_paterno ,
				'email'                => $email,
                'telefono'             => $telefono,
                'origen'               => $origen,
                'fecha_registro'       => $fecha_de_registro,
                'tutor_email'          => $tutor_email
        );
        
        //$sql="INSERT INTO registros($id_campus,$id_nivel_interes,$id_programa_interes,$id_modalidad,$id_periodo,$nombre,$apellido_paterno,$email,$telefono,$origen,$fecha_de_registro)";
        //$query=$this->db->query($sql);
        $this->db->insert('registros', $data);
        
    }

}
