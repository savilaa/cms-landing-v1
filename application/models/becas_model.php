<?php
class Becas_model extends CI_Model {

  

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    
    
    function get_nivel_interes($id_campus){
        $sql = "SELECT nivel_interes, id_nivel_interes from concentrado_view WHERE id_campus=? GROUP BY nivel_interes";
        $query=$this->db->query($sql, array($id_campus));
        return $query;
    }

    
    function get_programa_interes($id_campus,$id_nivel_interes)
    {
        $sql = "SELECT id_programa_interes, programa_interes, id_modalidad, id_periodo from concentrado_view
WHERE id_campus=? AND id_nivel_interes=? GROUP BY programa_interes;";
        $query=$this->db->query($sql, array($id_campus,$id_nivel_interes));
        
        return $query;
         
    }
    
    
    function get_campus_by_interes($id_nivel_interes, $nivel_interes){
        $sql = "SELECT cv.id_campus, c.campus_estado, cv.id_modalidad, cv.id_periodo  from concentrado_view cv
                LEFT JOIN campus c USING(id_campus)
                WHERE id_nivel_interes=? AND nivel_interes=?";
        $query=$this->db->query($sql, array($id_nivel_interes,$nivel_interes));
        return $query;
    }
    
    
    function get_nivel_interes_by_campus_from_wordpress($id_campus){
        $sql = "SELECT nivel_interes as label, id_nivel_interes as value from concentrado_view 
                WHERE id_campus=? GROUP BY label;";
        $query=$this->db->query($sql, array($id_campus));
        
        return $query;
        
    }
    
    function get_programa_interes_by_nivel_interes($id_nivel_interes, $nivel_interes=null){
        $where="";
        if($nivel_interes!=null){
            $where=" AND nivel_interes='$nivel_interes'";
        }
        
        $sql = "SELECT nivel_interes, id_nivel_interes, programa_interes, id_programa_interes from concentrado_view 
                WHERE id_nivel_interes=? $where GROUP BY programa_interes;";
        $query=$this->db->query($sql, array($id_nivel_interes));
        
        return $query;
        
    }
    
    function get_programa_interes_by_nivel_interes_from_workpress($id_campus, $nivel_interes){
        $where="";
        if($nivel_interes!=null){
            $where=" AND nivel_interes='$nivel_interes'";
        }
        
        $sql = "SELECT nivel_interes, id_nivel_interes, programa_interes, id_programa_interes from concentrado_view 
                WHERE id_campus=? $where GROUP BY programa_interes;";
        $query=$this->db->query($sql, array($id_campus));
        
        return $query;
        
    }
    
    
    function get_campus_BY_programa_and_nivel($id_nivel_interes, $id_programa_interes){
        $sql = "SELECT cv.id_campus, c.campus_estado, cv.id_modalidad, cv.id_periodo  from concentrado_view cv
                LEFT JOIN campus c USING(id_campus)
                WHERE id_nivel_interes=? AND id_programa_interes=? GROUP BY campus_estado;";
        $query=$this->db->query($sql, array($id_nivel_interes,$id_programa_interes));
        
        $data['existe']=$query->result_array();
        
        $sql = "SELECT cv.id_campus, c.campus_estado  from concentrado_view cv
                LEFT JOIN campus c USING(id_campus)
                WHERE cv.id_campus NOT IN(SELECT cv.id_campus from concentrado_view cv
                                            WHERE id_nivel_interes=? AND id_programa_interes=? GROUP BY campus
                                        )
                GROUP BY campus_estado ";
        $query=$this->db->query($sql, array($id_nivel_interes,$id_programa_interes));
        
        $data['noexiste']=$query->result_array();
        
        return $data;
    }
    
    function get_campus($id_campus=null)
    {
        $where="";
        if($id_campus!=null){
            $where=" WHERE id_campus=$id_campus";
        }
        $sql = "SELECT * from campus $where
                GROUP BY campus_estado";
        $query=$this->db->query($sql);

        return $query;

    }
    
    
    function get_campus_from_estado($estado=null)
    {
        $where="";
        if($estado!=null){
            $where=" WHERE campus_estado LIKE '%$estado%'";
        }
        $sql = "SELECT * from campus $where
                GROUP BY campus_estado";
        $query=$this->db->query($sql);

        return $query;

    }
    
    
    function get_progrma_BY_campus_and_nivel($id_nivel_interes,$id_campus){
        $sql = "SELECT nivel_interes, id_nivel_interes, programa_interes, id_programa_interes, id_modalidad, id_periodo from concentrado_view 
                WHERE id_nivel_interes=? AND id_campus=? GROUP BY programa_interes;";
        $query=$this->db->query($sql, array($id_nivel_interes,$id_campus));
        
        return $query;
    }
    
    
    function get_progrma_BY_campus_and_nivel_from_wordpress($id_nivel_interes,$id_campus){
        $sql = "SELECT programa_interes as label, id_programa_interes as value, id_modalidad, id_periodo from concentrado_view 
                WHERE id_nivel_interes=? AND id_campus=? GROUP BY programa_interes;";
        $query=$this->db->query($sql, array($id_nivel_interes,$id_campus));
        
        return $query;
    }
    
    
    function insert_masthead($id_campus,$id_nivel_interes,$id_programa_interes,$id_modalidad,$id_periodo,$nombre,$apellido_paterno,$email,$telefono,$origen){
       date_default_timezone_set('America/Mexico_City');
              
              $now   = new DateTime('NOW');
              $fecha_de_registro =  $now->format('Y/m/d H:i'); 
        
        $data = array(
				'id_campus'            => $id_campus,
				'id_nivel_interes'     => $id_nivel_interes,
				'id_programa_interes'  => $id_programa_interes,
				'id_modalidad'         => $id_modalidad ,
				'id_periodo'           => $id_periodo,
				'nombre'               => $nombre,
				'apellido_paterno'     => $apellido_paterno ,
				'email'                => $email,
                'telefono'             => $telefono,
                'origen'               => $origen,
                'fecha_registro'   => $fecha_de_registro
        );
        
        //$sql="INSERT INTO registros($id_campus,$id_nivel_interes,$id_programa_interes,$id_modalidad,$id_periodo,$nombre,$apellido_paterno,$email,$telefono,$origen,$fecha_de_registro)";
        //$query=$this->db->query($sql);
        $this->db->insert('registros_masthead', $data);
        
    }
    
    
    function insert_becas($id_campus,$id_nivel_interes,$id_programa_interes,$id_periodo,$id_modalidad,$nombre,$apellido_paterno,$email,$telefono,$origen,$detalle_origen,$tutor_email){
       date_default_timezone_set('America/Mexico_City');
              
              $now   = new DateTime('NOW');
              $fecha_de_registro =  $now->format('Y/m/d H:i'); 
        
        $data = array(
				'id_campus'            => $id_campus,
				'id_nivel_interes'     => $id_nivel_interes,
				'id_programa_interes'  => $id_programa_interes,
				'nombre'               => $nombre,
				'apellido_paterno'     => $apellido_paterno ,
				'email'                => $email,
                'telefono'             => $telefono,
                'origen'               => $origen,
                'id_periodo'           => $id_periodo,
                'id_modalidad'         => $id_modalidad,
                'detalle_origen'       => $detalle_origen,
                'fecha_registro'       => $fecha_de_registro,
                'tutor_email'          => $tutor_email
        );
        
         $this->db->insert('landing_becas', $data);
        
    }
    
    function get_mapa($id_campus){
        $sql = "SELECT latitud, longuitud FROM campus 
                WHERE id_campus=? LIMIT 1";
        $query=$this->db->query($sql, array($id_campus));
        
        return $query;
    }
    
    function get_cp(){
        $sql = "SELECT id_aux_cp, cp, colonia FROM aux_cp
                     WHERE status_coordenadas IS NULL OR status_coordenadas!=1";
                     
        $query=$this->db->query($sql);
        return $query;
    }
    
    function insert_coordenadas($latitud,$longitud,$latitud_northeast,$longitud_northeast,$latitud_southwest,$longitud_southwest,$id_cp){
        $data = array(
				'latitud'              => $latitud,
				'longitud'             => $longitud,
				'latitud_northeast'    => $latitud_northeast,
				'longitud_northeast'   => $longitud_northeast,
				'latitud_southwest'    => $latitud_southwest,
				'longitud_southwest'   => $longitud_southwest,
                'id_cp'                 => $id_cp
        );
        
        $this->db->insert('coordenadas_cp', $data);
        
    }
    
    function update_aux_cp_by_insert_coordenadas($id_cp){
         $sql = "UPDATE aux_cp SET status_coordenadas=1
                     WHERE id_aux_cp=?";
                     
        $query=$this->db->query($sql,array($id_cp));
        return $query;
    }
    
    
    function get_colonia_by_cp($cp){
        $sql = "SELECT acp.id_aux_cp, acp.colonia, ccp.latitud, ccp.longitud FROM aux_cp acp
                    INNER JOIN coordenadas_cp ccp ON acp.id_aux_cp=ccp.id_cp
                     WHERE acp.cp=?";
                     
        $query=$this->db->query($sql,array($cp));
        return $query;
    }
    /*
    metodo : get_ofertas_por_nivel
    obtiene los datos de
    vars : obj #datos_landig
    autor : chava, anel
    */
    function get_ofertas_por_nivel($id_nivel){
        $sql = "SELECT nivel_interes, id_nivel_interes, programa_interes, id_programa_interes from landing 
                WHERE id_nivel_interes=? AND id_campus=? GROUP BY programa_interes;";
        $query=$this->db->query($sql, array($id_nivel_interes,$id_campus));
        return $query;
    }  
    
}    