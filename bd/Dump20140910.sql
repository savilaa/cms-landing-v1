CREATE DATABASE  IF NOT EXISTS `page_landing` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `page_landing`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: page_landing
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `areas_academicas`
--

DROP TABLE IF EXISTS `areas_academicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas_academicas` (
  `id_area_academica` int(11) NOT NULL AUTO_INCREMENT,
  `area_academica` varchar(50) NOT NULL,
  `imagen_area_academica` varchar(45) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_nivel_interes` int(11) NOT NULL,
  PRIMARY KEY (`id_area_academica`,`id_nivel_interes`),
  KEY `fk_areas_academicas_nivel_interes1_idx` (`id_nivel_interes`),
  CONSTRAINT `fk_areas_academicas_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas_academicas`
--

LOCK TABLES `areas_academicas` WRITE;
/*!40000 ALTER TABLE `areas_academicas` DISABLE KEYS */;
INSERT INTO `areas_academicas` VALUES (9,'Licenciatura','8b4fe-licenciatura.png','activo','2014-09-10 22:21:37',4),(10,'Ingenieria','60e36-ingenieria.png','activo','2014-09-10 22:22:11',4),(11,'Licenciatura','81cb3-licenciatura.png','activo','2014-09-10 22:23:47',2),(12,'Ingenieria','c038a-ingenieria.png','activo','2014-09-10 22:24:21',2);
/*!40000 ALTER TABLE `areas_academicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beneficios`
--

DROP TABLE IF EXISTS `beneficios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beneficios` (
  `id_beneficios` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(45) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `content` longtext NOT NULL,
  `valor` varchar(2) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_nivel_interes` int(11) NOT NULL,
  PRIMARY KEY (`id_beneficios`,`id_nivel_interes`),
  KEY `fk_beneficios_nivel_interes1_idx` (`id_nivel_interes`),
  CONSTRAINT `fk_beneficios_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beneficios`
--

LOCK TABLES `beneficios` WRITE;
/*!40000 ALTER TABLE `beneficios` DISABLE KEYS */;
INSERT INTO `beneficios` VALUES (1,'1f67a-circulito1.jpg','Plan de estudios flexible:','Estudia lo que en verdad te gusta bajo un plan de estudios que se adapte a tus necesidades de aprendizaje.','1','activo','2014-09-10 22:06:27',1),(2,'d2c58-circulito2.jpg','Certificaciones:','Al ingresar a Preparatoria Tecmilenio obtendrás una certificación de Microsoft, pues es una gran ventaja al presentar ideas.','2','activo','2014-09-10 22:07:47',1),(3,'eced6-circulito3.jpg','Intercambios Estudiantiles:','Contamos con programas que te ayudaran a conquistar el mundo. ¡Conócelos!','3','activo','2014-09-10 22:08:50',1),(4,'955c8-circulito4.png','Compite para ganar:','Comprende tu entorno, rompe tus viejas creencias y conoce tus verdaderas capacidades para ser un líder.','4','activo','2014-09-10 22:09:30',1),(5,'3f916-circulito1.png','Plan de estudios flexible:','Estudia lo quieres, puedes seleccionar el 40% de tus materias, vinculadas a tu carrera profesional. ','1','activo','2014-09-10 22:55:12',4),(6,'54614-circulito2.jpg','Certificados de especialidad','Las 3 certificaciones provienen del plan de estudios y se relacionan con tu carrera, especialidad y tus gustos.','2','activo','2014-09-10 22:56:09',4),(7,'eff8b-circulito3.jpg','Diversidad de licenciaturas','La mejor oferta académica para seleccionar de entre 15 licenciaturas e ingenierías y convertirte en un experto.','3','activo','2014-09-10 22:56:57',4),(8,'2a6d9-circulito4.png','Felicidad y bienestar','Con los principios de la psicología positiva, te desarrollarás en un ambiente universitario de bienestar y felicidad.','4','activo','2014-09-10 22:57:47',4);
/*!40000 ALTER TABLE `beneficios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id_campus` int(11) NOT NULL AUTO_INCREMENT,
  `clave_campus` varchar(45) NOT NULL,
  `campus` varchar(40) NOT NULL,
  `campus_estado` varchar(60) NOT NULL,
  `Ing` varchar(30) NOT NULL,
  `lat` varchar(30) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_campus`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (1,'1101','Las Torres','Nuevo León | Campus Las Torres','25.6280672018859','-100.303914467736','activo','2014-09-09 18:06:26'),(2,'1102','San Nicolás','Nuevo León | Campus San Nicolás','25.7440019','-100.2829742','activo','2014-09-09 18:06:26'),(3,'1105','Cumbres','Nuevo León | Campus Cumbres','25.7426204','-100.4072071','activo','2014-09-09 18:06:26'),(4,'1106','Guadalupe','Nuevo León | Campus Guadalupe','25.6565754','-100.2073717','activo','2014-09-09 18:06:26'),(5,'1201','Cuautitlán','Edo. Mex. | Campus Cuautitlán','19.6895205','-99.2113669','activo','2014-09-09 18:06:26'),(6,'1202','Ferrería','D.F. | Campus Ferrería','19.4992261','-99.1755585','activo','2014-09-09 18:06:26'),(7,'1300','En Línea','Campus en Línea','0','0','activo','2014-09-09 18:06:26'),(8,'1401','Guadalajara','Jalisco | Campus Guadalajara','20.6396594','-103.3117846','activo','2014-09-09 18:06:26'),(9,'1403','Zapopan','Jalisco | Campus Zapopan','20.7365812\n','-103.3723895\n','activo','2014-09-09 18:06:26'),(10,'1502','Chihuahua','Chihuahua | Campus Chihuahua','28.6315449','-106.0748312\n','activo','2014-09-09 18:06:26'),(11,'1601','Culiacán','Sinaloa | Campus Culiacán','24.8048499','-107.385498','activo','2014-09-09 18:06:26'),(12,'1603','Mazatlán','Sinaloa | Campus Mazatlán','23.2761101','-106.4492517','activo','2014-09-09 18:06:26'),(13,'1604','Los Mochis','Sinaloa | Campus Los Mochis','25.8064819','-109.0125428','activo','2014-09-09 18:06:26'),(14,'1701','Cd. Obregón','Sonora | Campus Cd. Obregón','27.4904944','-109.9378606','activo','2014-09-09 18:06:26'),(15,'2101','Ciudad Juárez','Chihuahua | Campus Ciudad Juárez','31.67498718','-106.3874613\n','activo','2014-09-09 18:06:26'),(16,'2102','San Luis Potosí','S.L.P | Campus San Luis Potosi','22.1564699','-100.9855409','activo','2014-09-09 18:06:26'),(17,'2103','Laguna','Coahulia | Campus Laguna','25.6047104','-103.4120699','activo','2014-09-09 18:06:26'),(18,'2104','Durango','Durango | Campus Durango','23.9837992','-104.6685896','activo','2014-09-09 18:06:26'),(19,'2106','Matamoros','Tamaulipas | Campus Matamoros','25.8887715','-97.5602263','activo','2014-09-09 18:06:26'),(20,'2107','Reynosa','Tamaulipas | Reynosa','26.0833333','-98.2833333','activo','2014-09-09 18:06:26'),(21,'2108','Nuevo Laredo','Tamaulipas | Campus Nuevo Laredo','27.4216286','-99.517867','activo','2014-09-09 18:06:26'),(22,'2201','Toluca','Edo. Mex. | Campus Toluca','19.2538774\n','-99.6273495\n','activo','2014-09-09 18:06:26'),(23,'2205','Querétaro','Querétaro | Campus Querétaro','20.3928434','-100.0051889','activo','2014-09-09 18:06:26'),(24,'2301','Veracruz','Veracruz | Campus Veracruz','19.1063237\n','-96.1515352','activo','2014-09-09 18:06:26'),(25,'2302','Villahermosa','Tabasco | Campus Villahermosa','18.023119','-92.9289067\n','activo','2014-09-09 18:06:26'),(26,'2303','Puebla','Puebla | Campus Puebla','19.0798741\n','-98.2252284','activo','2014-09-09 18:06:26'),(27,'2305','Morelia','Michoacán | Campus Morelia','19.72768','-101.105322','activo','2014-09-09 18:06:26'),(28,'2307','Cuernavaca','Morelos | Campus Cuernavaca','18.8712917','-99.2162139','activo','2014-09-09 18:06:26'),(29,'2308','Mérida','Yucatán | Campus Mérida','21.038134','-89.6614087','activo','2014-09-09 18:06:26'),(30,'2309','Cancún','Q. Roo| Campus Cancún','21.1324201','-86.8270041\n','activo','2014-09-09 18:06:26'),(31,'2401','Hermosillo','Sonora | Campus Hermosillo','29.1444452','-110.9571833\n','activo','2014-09-09 18:06:26');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_origen`
--

DROP TABLE IF EXISTS `detalle_origen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_origen` (
  `id_detalle_origen` int(11) NOT NULL AUTO_INCREMENT,
  `clave_detalle_origen` int(2) NOT NULL,
  `detalle_origen` varchar(50) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_detalle_origen`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_origen`
--

LOCK TABLES `detalle_origen` WRITE;
/*!40000 ALTER TABLE `detalle_origen` DISABLE KEYS */;
INSERT INTO `detalle_origen` VALUES (1,34,'Becas Talento','activo','2014-09-08 11:38:00'),(2,22,'gl preparatoria','activo','2014-09-09 18:28:05'),(3,23,'gl profesional','activo','2014-09-09 18:28:05'),(4,24,'gl ejecutivas','activo','2014-09-09 18:28:05'),(5,25,'gl maestrias','activo','2014-09-09 18:28:05'),(6,26,'gl carreras en linea','activo','2014-09-09 18:28:05'),(7,27,'gl maestrias en linea','activo','2014-09-09 18:28:05'),(8,28,'fb preparatoria','activo','2014-09-09 18:28:05'),(9,29,'fb profesional','activo','2014-09-09 18:28:05'),(10,30,'fb ejecutivas','activo','2014-09-09 18:28:05'),(11,31,'fb maestrias','activo','2014-09-09 18:28:05'),(12,32,'fb carreras en linea','activo','2014-09-09 18:28:05'),(13,33,'fb maestrias en linea','activo','2014-09-09 18:28:05'),(14,36,'masthead','activo','2014-09-09 18:28:05'),(15,37,'mp_profesionales','activo','2014-09-09 18:28:05'),(16,38,'mp_maestrias','activo','2014-09-09 18:28:05'),(17,39,'me_profesionales','activo','2014-09-09 18:28:05'),(18,40,'me_maestrias','activo','2014-09-09 18:28:05'),(19,41,'sedena','activo','2014-09-09 18:28:05');
/*!40000 ALTER TABLE `detalle_origen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formulario`
--

DROP TABLE IF EXISTS `formulario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formulario` (
  `id_formulario` int(11) NOT NULL AUTO_INCREMENT,
  `formulario_nombre` varchar(45) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_formulario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formulario`
--

LOCK TABLES `formulario` WRITE;
/*!40000 ALTER TABLE `formulario` DISABLE KEYS */;
INSERT INTO `formulario` VALUES (2,'forum 1','activo','2014-09-22 17:00:00');
/*!40000 ALTER TABLE `formulario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `landing`
--

DROP TABLE IF EXISTS `landing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `landing` (
  `id_landing` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(80) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `frase` varchar(80) NOT NULL,
  `img_principal` varchar(50) DEFAULT NULL,
  `beneficios_titulo` varchar(60) NOT NULL,
  `video` varchar(45) NOT NULL,
  `codigo_conversion` varchar(45) NOT NULL,
  `label` varchar(45) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_formulario` int(11) NOT NULL,
  `id_origen` int(11) NOT NULL,
  `id_detalle_origen` int(11) NOT NULL,
  `id_nivel_interes` int(11) NOT NULL DEFAULT '0',
  `descripcion` varchar(200) DEFAULT NULL,
  `id_area_academica` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_landing`,`id_formulario`,`id_origen`,`id_detalle_origen`,`id_nivel_interes`),
  KEY `fk_landing_origen1_idx` (`id_origen`),
  KEY `fk_landing_fomulario1_idx` (`id_formulario`),
  KEY `fk_landing_detalle_origen1_idx` (`id_detalle_origen`),
  KEY `fk_landing_nivel_interes1_idx` (`id_nivel_interes`),
  KEY `fk_landing_areas_academicas1_idx` (`id_area_academica`),
  CONSTRAINT `fk_landing_detalle_origen1` FOREIGN KEY (`id_detalle_origen`) REFERENCES `detalle_origen` (`id_detalle_origen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_landing_fomulario1` FOREIGN KEY (`id_formulario`) REFERENCES `formulario` (`id_formulario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_landing_origen1` FOREIGN KEY (`id_origen`) REFERENCES `origen` (`id_origen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_landing_areas_academicas1` FOREIGN KEY (`id_area_academica`) REFERENCES `areas_academicas` (`id_area_academica`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_landing_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `landing`
--

LOCK TABLES `landing` WRITE;
/*!40000 ALTER TABLE `landing` DISABLE KEYS */;
INSERT INTO `landing` VALUES (30,'tjekkj','dfkjgkj','dkfjlkdfjs','96c14-foto_sedena.png','','jkdkajfd','kdjjlfñkakjd','ijkldñkfjldsk','activo','2014-09-10 18:05:42',2,2,12,6,NULL,0),(31,'/gl/prepa','Prepa Tecmilenio','Semestral y Tetramestral ','12014-head-universal.png','Descubre un mundo de oportunidades:','xE-9ctDgOuc','974482803','xgVcCPWusAoQ89rV0AM','activo','2014-09-10 18:13:23',2,2,2,1,NULL,0),(32,'jldskjfa','kjlkjñk','kjlkj','61664-head-universal.png','kljlkjlkj','kjlkjkl','jhkjhkj','jhkjhkjh','activo','2014-09-10 18:53:31',2,2,8,1,NULL,0),(36,'jkadfklja','kjalfdksj','dlkjalñfkd','87ac1-carrera_enlinea.png','kdjlñakj','dkjadñfkl','lkdjfalñkj','dkljafdlk','activo','2014-09-10 23:09:31',2,2,11,3,'dkjfalñkd',9);
/*!40000 ALTER TABLE `landing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modalidad`
--

DROP TABLE IF EXISTS `modalidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modalidad` (
  `id_modalidad` int(11) NOT NULL AUTO_INCREMENT,
  `clave_modalidad` varchar(5) NOT NULL,
  `modalidad` varchar(50) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_periodo` int(11) NOT NULL,
  PRIMARY KEY (`id_modalidad`,`id_periodo`),
  KEY `fk_modalidad_periodo1_idx` (`id_periodo`),
  CONSTRAINT `fk_modalidad_periodo1` FOREIGN KEY (`id_periodo`) REFERENCES `periodo` (`id_periodo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modalidad`
--

LOCK TABLES `modalidad` WRITE;
/*!40000 ALTER TABLE `modalidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `modalidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nivel_interes`
--

DROP TABLE IF EXISTS `nivel_interes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nivel_interes` (
  `id_nivel_interes` int(11) NOT NULL AUTO_INCREMENT,
  `clave_nivel_interes` varchar(45) NOT NULL,
  `nivel_interes` varchar(100) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_nivel_interes`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nivel_interes`
--

LOCK TABLES `nivel_interes` WRITE;
/*!40000 ALTER TABLE `nivel_interes` DISABLE KEYS */;
INSERT INTO `nivel_interes` VALUES (1,'1','Preparatoria','activo','2014-09-09 18:30:28'),(2,'2','Profesional ejecutiva','activo','2014-09-09 18:30:28'),(3,'3','Maestría','activo','2014-09-09 18:30:28'),(4,'2','Profesional','activo','2014-09-09 18:30:28'),(5,'4','Ejecutiva en linea','activo','2014-09-09 18:30:28'),(6,'3','Maestría en línea','activo','2014-09-09 18:30:28'),(7,'2','Profesional en línea','activo','2014-09-09 18:30:28');
/*!40000 ALTER TABLE `nivel_interes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ofrece`
--

DROP TABLE IF EXISTS `ofrece`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ofrece` (
  `id_ofrece` int(11) NOT NULL,
  `tipo` enum('unico','otro','ambos') NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_campus` int(11) NOT NULL,
  `id_programas_interes` int(11) NOT NULL,
  `id_nivel_interes` int(11) NOT NULL,
  `id_modalidad` int(11) NOT NULL,
  PRIMARY KEY (`id_ofrece`,`id_campus`,`id_programas_interes`,`id_nivel_interes`,`id_modalidad`),
  KEY `fk_ofrece_modalidad1_idx` (`id_modalidad`),
  KEY `fk_ofrece_campus1_idx` (`id_campus`),
  KEY `fk_ofrece_programa_interes1_idx` (`id_programas_interes`),
  KEY `fk_ofrece_nivel_interes1_idx` (`id_nivel_interes`),
  CONSTRAINT `fk_ofrece_campus1` FOREIGN KEY (`id_campus`) REFERENCES `campus` (`id_campus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ofrece_modalidad1` FOREIGN KEY (`id_modalidad`) REFERENCES `modalidad` (`id_modalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ofrece_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ofrece_programa_interes1` FOREIGN KEY (`id_programas_interes`) REFERENCES `programa_interes` (`id_programas_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ofrece`
--

LOCK TABLES `ofrece` WRITE;
/*!40000 ALTER TABLE `ofrece` DISABLE KEYS */;
/*!40000 ALTER TABLE `ofrece` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `origen`
--

DROP TABLE IF EXISTS `origen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `origen` (
  `id_origen` int(11) NOT NULL AUTO_INCREMENT,
  `clave_origen` int(2) NOT NULL,
  `origen` varchar(50) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_origen`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `origen`
--

LOCK TABLES `origen` WRITE;
/*!40000 ALTER TABLE `origen` DISABLE KEYS */;
INSERT INTO `origen` VALUES (2,1,'landing','activo','2014-09-10 17:00:00');
/*!40000 ALTER TABLE `origen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodo`
--

DROP TABLE IF EXISTS `periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodo` (
  `id_periodo` int(11) NOT NULL AUTO_INCREMENT,
  `clave_periodo` varchar(5) NOT NULL,
  `periodo` varchar(45) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_periodo`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodo`
--

LOCK TABLES `periodo` WRITE;
/*!40000 ALTER TABLE `periodo` DISABLE KEYS */;
INSERT INTO `periodo` VALUES (1,'1','Enero 2007','activo','2014-09-09 18:23:56'),(2,'2','Mayo 2007','activo','2014-09-09 18:23:56'),(3,'3','Agosto 2007','activo','2014-09-09 18:23:56'),(4,'4','Septiembre 2007','activo','2014-09-09 18:23:56'),(5,'5','Enero 2008','activo','2014-09-09 18:23:56'),(6,'6','Mayo 2008','activo','2014-09-09 18:23:56'),(7,'7','Agosto 2008','activo','2014-09-09 18:23:56'),(8,'8','Septiembre 2008','activo','2014-09-09 18:23:56'),(9,'9','Enero 2009','activo','2014-09-09 18:23:56'),(10,'10','Mayo 2009','activo','2014-09-09 18:23:56'),(11,'11','Agosto 2009','activo','2014-09-09 18:23:56'),(12,'12','Septiembre 2009','activo','2014-09-09 18:23:56'),(13,'13','Noviembre 2010','activo','2014-09-09 18:23:56'),(14,'14','Enero 2010','activo','2014-09-09 18:23:56'),(15,'15','Mayo 2010','activo','2014-09-09 18:23:56'),(16,'16','Agosto 2010','activo','2014-09-09 18:23:56'),(17,'17','Septiembre 2010','activo','2014-09-09 18:23:56'),(18,'18','Enero 2011','activo','2014-09-09 18:23:56'),(19,'19','Mayo 2011','activo','2014-09-09 18:23:56'),(20,'20','Agosto 2011','activo','2014-09-09 18:23:56'),(21,'21','Septiembre 2011','activo','2014-09-09 18:23:56'),(22,'22','Enero 2012','activo','2014-09-09 18:23:56'),(23,'23','Mayo 2012','activo','2014-09-09 18:23:56'),(24,'24','Agosto 2012','activo','2014-09-09 18:23:56'),(25,'25','Septiembre 2012','activo','2014-09-09 18:23:56'),(26,'26','Enero 2013','activo','2014-09-09 18:23:56'),(27,'27','Agosto 2013','activo','2014-09-09 18:23:56'),(28,'28','Septiembre 2013','activo','2014-09-09 18:23:56'),(29,'29','Mayo 2013','activo','2014-09-09 18:23:56'),(30,'30','Enero 2014','activo','2014-09-09 18:23:56'),(31,'31','Mayo 2014','activo','2014-09-09 18:23:56'),(32,'32','Agosto 2014','activo','2014-09-09 18:23:56'),(33,'33','Septiembre 2014','activo','2014-09-09 18:23:56'),(34,'34','Enero 2015','activo','2014-09-09 18:23:56'),(35,'35','Mayo 2015','activo','2014-09-09 18:23:56'),(36,'36','Agosto 2015','activo','2014-09-09 18:23:56'),(37,'37','Septiembre 2015','activo','2014-09-09 18:23:56'),(38,'38','Enero 2016','activo','2014-09-09 18:23:56'),(39,'39','Mayo 2016','activo','2014-09-09 18:23:56'),(40,'40','Agosto 2016','activo','2014-09-09 18:23:56'),(41,'41','Septiembre 2016','activo','2014-09-09 18:23:56'),(42,'42','Enero 2017','activo','2014-09-09 18:23:56'),(43,'43','Mayo 2017','activo','2014-09-09 18:23:56'),(44,'44','Agosto 2017','activo','2014-09-09 18:23:56'),(45,'45','Septiembre 2017','activo','2014-09-09 18:23:56'),(46,'46','Enero 2018','activo','2014-09-09 18:23:56'),(47,'47','Mayo 2018','activo','2014-09-09 18:23:56'),(48,'48','Agosto 2018','activo','2014-09-09 18:23:56'),(49,'49','Septiembre 2018','activo','2014-09-09 18:23:56'),(50,'50','Enero 2019','activo','2014-09-09 18:23:56'),(51,'51','Mayo 2019','activo','2014-09-09 18:23:56'),(52,'52','Agosto 2019','activo','2014-09-09 18:23:56'),(53,'53','Septiembre 2019','activo','2014-09-09 18:23:56'),(54,'54','Enero 2020','activo','2014-09-09 18:23:56'),(55,'55','Mayo 2020','activo','2014-09-09 18:23:56'),(56,'56','Agosto 2020','activo','2014-09-09 18:23:56'),(57,'57','Septiembre 2020','activo','2014-09-09 18:23:56'),(58,'58','Enero 2021','activo','2014-09-09 18:23:56'),(59,'59','Mayo 2021','activo','2014-09-09 18:23:56'),(60,'60','Agosto 2021','activo','2014-09-09 18:23:56'),(61,'61','Septiembre 2021','activo','2014-09-09 18:23:56'),(62,'62','Enero 2022','activo','2014-09-09 18:23:56'),(63,'63','Mayo 2022','activo','2014-09-09 18:23:56'),(64,'64','Agosto 2022','activo','2014-09-09 18:23:56'),(65,'65','Septiembre 2022','activo','2014-09-09 18:23:56'),(66,'66','Enero 2023','activo','2014-09-09 18:23:56'),(67,'67','Mayo 2023','activo','2014-09-09 18:23:56'),(68,'68','Agosto 2023','activo','2014-09-09 18:23:56'),(69,'69','Septiembre 2023','activo','2014-09-09 18:23:56'),(70,'70','Enero 2024','activo','2014-09-09 18:23:56'),(71,'71','Mayo 2024','activo','2014-09-09 18:23:56'),(72,'72','Agosto 2024','activo','2014-09-09 18:23:56'),(73,'73','Septiembre 2024','activo','2014-09-09 18:23:56'),(74,'74','Enero 2025','activo','2014-09-09 18:23:56'),(75,'75','Mayo 2025','activo','2014-09-09 18:23:56'),(76,'76','Agosto 2025','activo','2014-09-09 18:23:56'),(77,'77','Septiembre 2025','activo','2014-09-09 18:23:56'),(78,'78','Enero 2026','activo','2014-09-09 18:23:56'),(79,'79','Mayo 2026','activo','2014-09-09 18:23:56'),(80,'80','Agosto 2026','activo','2014-09-09 18:23:56'),(81,'81','Septiembre 2026','activo','2014-09-09 18:23:56'),(82,'82','Enero 2027','activo','2014-09-09 18:23:56'),(83,'83','Mayo 2027','activo','2014-09-09 18:23:56'),(84,'84','Agosto 2027','activo','2014-09-09 18:23:56'),(85,'85','Septiembre 2027','activo','2014-09-09 18:23:56'),(86,'86','Enero 2028','activo','2014-09-09 18:23:56'),(87,'87','Mayo 2028','activo','2014-09-09 18:23:56'),(88,'88','Agosto 2028','activo','2014-09-09 18:23:56'),(89,'89','Septiembre 2028','activo','2014-09-09 18:23:56'),(90,'90','Enero 2029','activo','2014-09-09 18:23:56'),(91,'91','Mayo 2029','activo','2014-09-09 18:23:56'),(92,'92','Agosto 2029','activo','2014-09-09 18:23:56'),(93,'93','Septiembre 2029','activo','2014-09-09 18:23:56'),(94,'94','Enero 2030','activo','2014-09-09 18:23:56'),(95,'95','Mayo 2030','activo','2014-09-09 18:23:56'),(96,'96','Agosto 2030','activo','2014-09-09 18:23:56'),(97,'97','Septiembre 2030','activo','2014-09-09 18:23:56');
/*!40000 ALTER TABLE `periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programa_interes`
--

DROP TABLE IF EXISTS `programa_interes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programa_interes` (
  `id_programas_interes` int(11) NOT NULL AUTO_INCREMENT,
  `clave_programa_interes` varchar(5) NOT NULL,
  `programa_interes` varchar(50) NOT NULL,
  `url_programa_interes` varchar(150) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_nivel_interes` int(11) NOT NULL,
  `id_area_academica` int(11) NOT NULL,
  PRIMARY KEY (`id_programas_interes`,`id_nivel_interes`,`id_area_academica`),
  KEY `fk_programa_interes_nivel_interes1_idx` (`id_nivel_interes`),
  KEY `fk_programa_interes_areas_academicas1_idx` (`id_area_academica`),
  CONSTRAINT `fk_programa_interes_areas_academicas1` FOREIGN KEY (`id_area_academica`) REFERENCES `areas_academicas` (`id_area_academica`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_programa_interes_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programa_interes`
--

LOCK TABLES `programa_interes` WRITE;
/*!40000 ALTER TABLE `programa_interes` DISABLE KEYS */;
INSERT INTO `programa_interes` VALUES (1,'109','Licenciatura en Enfermeria','http://tecmilenio.mx/carrera-profesional/licenciatura-enfermeria/','activo','2014-09-10 22:33:05',4,9),(2,'68','Ing. en Desarrollo de Software','http://tecmilenio.mx/carrera-profesional/ingenieria-desarrollo-software/','activo','2014-09-10 22:41:15',4,10),(3,'70','Ing. en Mecatrónica','http://tecmilenio.mx/carrera-profesional/ingenieria-mecatronica/','activo','2014-09-10 22:41:58',4,10),(4,'74','Ing. en Sistemas de Computación Administrativa','http://tecmilenio.mx/carrera-profesional/ingenieria-sistemas-computacion-administrativa/','activo','2014-09-10 22:44:19',4,10),(5,'77','Ing. Industrial','http://tecmilenio.mx/carrera-profesional/ingenieria-industrial/','activo','2014-09-10 22:45:07',4,10),(6,'96','Lic. en Administración de Empresas','http://tecmilenio.mx/carrera-profesional/licenciatura-administracion-empresas/','activo','2014-09-10 22:45:55',4,9),(7,'98','Lic. en Administración Financiera','http://tecmilenio.mx/carrera-profesional/licenciatura-administracion-financiera/','activo','2014-09-10 22:46:43',4,9),(8,'120','Lic. en Administración Hotelera y Turística','http://tecmilenio.mx/carrera-profesional/licenciatura-administracion-hotelera-turistica/','activo','2014-09-10 22:47:35',4,9),(9,'101','Lic. en Comercio Internacional','http://tecmilenio.mx/carrera-profesional/licenciatura-comercio-internacional/','activo','2014-09-10 22:48:17',4,9),(10,'103','Lic. en Derecho','http://tecmilenio.mx/carrera-profesional/licenciatura-derecho/','activo','2014-09-10 22:48:50',4,9),(11,'106','Lic. en Diseño Gráfico y Animación','http://tecmilenio.mx/carrera-profesional/licenciatura-diseno-grafico-animacion/','activo','2014-09-10 22:50:04',4,9),(12,'110','Lic. en Gastronomía','http://tecmilenio.mx/carrera-profesional/licenciatura-gastronomia/','activo','2014-09-10 22:51:03',4,9),(13,'113','Lic. en Mercadotecnia','http://tecmilenio.mx/carrera-profesional/licenciatura-mercadotecnia/','activo','2014-09-10 22:51:40',4,9),(14,'116','Lic. en Nutrición','http://tecmilenio.mx/carrera-profesional/licenciatura-nutricion/','activo','2014-09-10 22:52:20',4,9),(15,'117','Lic. en Psicología','http://tecmilenio.mx/carrera-profesional/licenciatura-psicologia/','activo','2014-09-10 22:52:52',4,9);
/*!40000 ALTER TABLE `programa_interes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro`
--

DROP TABLE IF EXISTS `registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro` (
  `id_registro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `email` varchar(35) NOT NULL,
  `tutor_email` varchar(35) NOT NULL,
  `status` enum('activo','inactivo') NOT NULL DEFAULT 'activo',
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_campus` int(11) NOT NULL,
  `id_nivel_interes` int(11) NOT NULL,
  `id_programas_interes` int(11) NOT NULL,
  `id_origen` int(11) NOT NULL,
  `id_detalle_origen` int(11) NOT NULL,
  `id_periodo` int(11) NOT NULL,
  `id_modalidad` int(11) NOT NULL,
  PRIMARY KEY (`id_registro`,`id_campus`,`id_nivel_interes`,`id_programas_interes`,`id_origen`,`id_detalle_origen`,`id_periodo`,`id_modalidad`),
  KEY `fk_registro_campus1_idx` (`id_campus`),
  KEY `fk_registro_detalle_origen1_idx` (`id_detalle_origen`),
  KEY `fk_registro_origen1_idx` (`id_origen`),
  KEY `fk_registro_programa_interes1_idx` (`id_programas_interes`),
  KEY `fk_registro_periodo1_idx` (`id_periodo`),
  KEY `fk_registro_modalidad1_idx` (`id_modalidad`),
  KEY `fk_registro_nivel_interes1_idx` (`id_nivel_interes`),
  CONSTRAINT `fk_registro_campus1` FOREIGN KEY (`id_campus`) REFERENCES `campus` (`id_campus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_detalle_origen1` FOREIGN KEY (`id_detalle_origen`) REFERENCES `detalle_origen` (`id_detalle_origen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_modalidad1` FOREIGN KEY (`id_modalidad`) REFERENCES `modalidad` (`id_modalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_nivel_interes1` FOREIGN KEY (`id_nivel_interes`) REFERENCES `nivel_interes` (`id_nivel_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_origen1` FOREIGN KEY (`id_origen`) REFERENCES `origen` (`id_origen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_periodo1` FOREIGN KEY (`id_periodo`) REFERENCES `periodo` (`id_periodo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_programa_interes1` FOREIGN KEY (`id_programas_interes`) REFERENCES `programa_interes` (`id_programas_interes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro`
--

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;
/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `table1`
--

DROP TABLE IF EXISTS `table1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table1` (
  `idtable1` int(11) NOT NULL,
  PRIMARY KEY (`idtable1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table1`
--

LOCK TABLES `table1` WRITE;
/*!40000 ALTER TABLE `table1` DISABLE KEYS */;
/*!40000 ALTER TABLE `table1` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-10 18:13:26
